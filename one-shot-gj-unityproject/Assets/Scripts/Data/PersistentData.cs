namespace Data
{
    public class PersistentData
    {
        private static PersistentData instance;

        public readonly IntField MaxScore = new(nameof(MaxScore));

        public static PersistentData Instance
        {
            get { return instance ??= new PersistentData(); }
        }
    }
}