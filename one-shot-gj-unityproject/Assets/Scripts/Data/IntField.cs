using UnityEngine;

namespace Data
{
    public class IntField : PlayerPrefsField<int>
    {
        public IntField(string name) : base(name)
        {
        }

        protected override void WriteToPlayerPrefs(string name, int value)
        {
            PlayerPrefs.SetInt(name, value);
        }

        protected override int ReadFromPlayerPrefs(string name)
        {
            return PlayerPrefs.GetInt(name);
        }
    }
}