using UnityEngine;

namespace Data
{
    public abstract class PlayerPrefsField<T>
    {
        private readonly string _name;
        private bool _isRead;
        private T _value;

        public PlayerPrefsField(string name)
        {
            _name = name;
            _isRead = false;
        }

        public T Value
        {
            get
            {
                if (!_isRead)
                {
                    _value = ReadFromPlayerPrefs(_name);
                    PlayerPrefs.Save();
                    _isRead = true;
                }

                return _value;
            }
            set
            {
                _value = value;
                WriteToPlayerPrefs(_name, value);
            }
        }

        protected abstract void WriteToPlayerPrefs(string name, T value);
        protected abstract T ReadFromPlayerPrefs(string name);
    }
}