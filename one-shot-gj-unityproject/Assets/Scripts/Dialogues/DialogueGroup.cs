using System.Collections.Generic;
using UnityEngine;

namespace Dialogues
{
    [CreateAssetMenu]
    public class DialogueGroup : ScriptableObject
    {
        public List<DialogueDefinition> Dialogues;
    }
}