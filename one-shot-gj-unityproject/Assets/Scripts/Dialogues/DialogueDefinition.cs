using System.Collections.Generic;
using UnityEngine;

namespace Dialogues
{
    [CreateAssetMenu]
    public class DialogueDefinition : ScriptableObject
    {
        [TextAreaAttribute(3,15)]
        public List<string> Phrases;
    }
}