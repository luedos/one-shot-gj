using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CircleDrawer : MonoBehaviour
{
    LineRenderer m_LineRenderer;
    private void Awake()
    {
        m_LineRenderer = GetComponent<LineRenderer>();
    }
    private void Start()
    {
        DrawCircle(100, 1);
    }
    public void Show() => m_LineRenderer.enabled = true;
    public void Hide() => m_LineRenderer.enabled = false;
    public void SetColor(Color color)
    {
        m_LineRenderer.startColor = color;
        m_LineRenderer.endColor = color;
    }
    public void DrawCircle(int steps,float radius)
    {
        m_LineRenderer.positionCount = steps;
        for (int currentStep = 0; currentStep < steps; currentStep++)
        {
            float progress = (float)currentStep / steps;
            float currentRadian = progress * 2 * Mathf.PI;
            float xScaled = Mathf.Cos(currentRadian);
            float yScaled = Mathf.Sin(currentRadian);

            float x = xScaled * radius;
            float y = yScaled * radius;
            Vector3 currentPosition = new Vector3(x, y, 0);
            m_LineRenderer.SetPosition(currentStep, currentPosition);
        }
    }
}
