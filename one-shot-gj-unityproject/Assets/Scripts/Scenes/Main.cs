using Ui;
using UnityEngine;

namespace Global
{
    public class Main : MonoBehaviour
    {
        public static Main Instance { get; private set; }

        private void Awake()
        {
            Instance = this;
        }

        private void Start()
        {
            UiManager.Instance.HideAll();
            UiManager.Instance.GetScreen<MainMenu>(ScreenType.MainMenu).Show();
            Time.timeScale = 1;
            AudioListener.volume = 100;
        }
    }
}