using Ui;
using UnityEngine;
using UnityEngine.SceneManagement;

namespace Scenes
{
    public class Boot : MonoBehaviour
    {
        public static bool IsLoaded;
        private static bool IsExternalLoadRequested;

        public static void Load()
        {
            IsExternalLoadRequested = true;
            SceneManager.LoadScene(SceneNames.Boot, LoadSceneMode.Additive);
        }

        private void Start()
        {
            IsLoaded = true;
            DontDestroyOnLoad(this);

            if (!IsExternalLoadRequested)
            {
                UiManager.Instance.GetScreen<LoadingScreen>(ScreenType.LoadingScreen).Show();
                SceneManager.LoadScene(SceneNames.Main);
            }
            else
            {
                SceneManager.UnloadSceneAsync(SceneNames.Boot);
            }
        }
    }
}