using UnityEngine;

namespace Scenes
{
    public class BootLoader : MonoBehaviour
    {
        private void Awake()
        {
            if (Application.isEditor && !Boot.IsLoaded)
            {
                Boot.Load();
            } 
        }
    }
}