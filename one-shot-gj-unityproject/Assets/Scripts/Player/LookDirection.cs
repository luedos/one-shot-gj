using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class LookDirection : MonoBehaviour
{
    public Transform m_Visual = null;
    public float m_RotationFadeOut = 5.0f;

    private Quaternion m_currentDirection = Quaternion.identity;

    // Start is called before the first frame update
    void Start()
    {
        m_currentDirection = transform.rotation;
    }

    // Update is called once per frame
    void Update()
    {
        Vector3 worldMousePos = Camera.main.ScreenToWorldPoint(Input.mousePosition);
        worldMousePos.z = transform.position.z;
        
        m_currentDirection = Quaternion.LookRotation(Vector3.forward, worldMousePos - transform.position);

        if (m_Visual != null)
        {
            m_Visual.rotation = Quaternion.Lerp(m_Visual.rotation, m_currentDirection, Time.deltaTime * m_RotationFadeOut);
        }
    }

    public Quaternion GetCurrentDirection()
    {
        return m_currentDirection;
    }
}
