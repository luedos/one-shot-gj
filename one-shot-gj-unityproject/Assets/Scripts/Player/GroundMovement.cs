using System.Collections;
using System.Collections.Generic;
using Unity.VisualScripting;
using UnityEngine;
using UnityEngine.InputSystem;

public class GroundMovement : MonoBehaviour
{
    [SerializeField] private float m_MoveSpeed = 10.0f;
    [SerializeField] private float m_VelocityFadeIn = 100.0f;
    [SerializeField] private float m_VelocityFadeOut = 50.0f;

    private Rigidbody2D m_rigidbody = null;
    private PlayerInput m_inputActions = null;
    private Vector2 m_inputDirection = Vector2.zero;

    private void Awake()
    {
        m_inputActions = new PlayerInput();
        m_rigidbody = GetComponent<Rigidbody2D>();
    }

    private void OnEnable()
    {
        m_inputActions.Movement.Walking.performed += OnMovementPerformed;
        m_inputActions.Movement.Walking.canceled += OnMovementCanceled;
        m_inputActions.Enable();
    }

    private void OnDisable()
    {
        m_inputActions.Disable();
        m_inputActions.Movement.Walking.performed -= OnMovementPerformed;
        m_inputActions.Movement.Walking.canceled -= OnMovementCanceled;

        m_inputDirection = Vector2.zero;
        m_rigidbody.velocity = Vector2.zero;
    }

    private void FixedUpdate()
    {
        Vector2 targetVelocity = m_inputDirection * m_MoveSpeed;
        Vector2 currentVelocity = m_rigidbody.velocity;
        float fade = targetVelocity.SqrMagnitude() > currentVelocity.SqrMagnitude()
            ? m_VelocityFadeIn
            : m_VelocityFadeOut;

        m_rigidbody.velocity = Vector2.Lerp(currentVelocity, targetVelocity, fade * Time.fixedDeltaTime);
    }
    public Vector2 GetMovementDirection() => m_inputDirection;
    private void OnMovementPerformed(InputAction.CallbackContext _context)
    {
        m_inputDirection = _context.ReadValue<Vector2>();
    }

    private void OnMovementCanceled(InputAction.CallbackContext _context)
    {
        m_inputDirection = Vector2.zero;
    }
}
