using System.Collections;
using System.Collections.Generic;
using System.Linq;
using TMPro;
using Unity.VisualScripting;
using UnityEngine;
using UnityEngine.InputSystem;
using static UnityEngine.GraphicsBuffer;

public class NetShooter : MonoBehaviour
{
    [SerializeField] private SpiderWeb m_SpiderWebPrefab = null;
    [SerializeField] private ImaginableNetVisual m_ImaginableWebPrefab = null;
    [SerializeField] private CircleDrawer m_SightPrefab = null;

    [SerializeField] private int m_MaxWebCount = 20;
    [SerializeField] private float m_MaxShotDistance = 10000.0f;

    [SerializeField] private TextMeshProUGUI m_WebLeftText = null;

    private PlayerInput m_inputActions = null;

    private GameObject m_firstPendingObject = null;
    private Vector2 m_firstPendingPoint = Vector2.zero;

    private NetInteractive m_aimedInteractive = null;
    private Vector2 m_aimedPos = Vector2.zero;
    private ImaginableNetVisual m_ImaginableWebHolder = null;
    private CircleDrawer m_MainSight = null;
    private CircleDrawer m_SecondarySight = null;

    private bool ResolveTraget(ref Vector2 _pos, ref GameObject _object, GameObject _ignore = null)
    {
        _object = null;

        int interactiveLayer = LayerMask.NameToLayer("InteractiveSnap");
        int wallLayer = LayerMask.NameToLayer("Wall");
        int interactiveLayerMask = 1 << interactiveLayer;
        int wallLayerMask = 1 << wallLayer;

        // Checking for possible targets
        {
            List<NetInteractive> interactives = new List<NetInteractive>();

            RaycastHit2D[] hits = Physics2D.RaycastAll(_pos, _pos, 0.0f, interactiveLayerMask);

            foreach (RaycastHit2D hit in hits)
            {
                if (hit.collider == null)
                {
                    continue;
                }

                NetInteractive target = FindTarget(hit.collider.gameObject);
                if (target == null || (_ignore != null && IsDescendent(_ignore.transform, target.transform)))
                {
                    continue;
                }

                Vector2 possiblePos = target.SnapPosition(_pos);
                if (CheckVisionForTarget(target, possiblePos))
                {
                    interactives.Add(target);
                }
            }

            if (interactives.Count > 0)
            {
                NetInteractive priorityInteractive = interactives[0];
                for (int interactiveIndex = 1; interactiveIndex < interactives.Count; ++interactiveIndex)
                {
                    NetInteractive possiblePriorityInteractive = interactives[interactiveIndex];
                    if (possiblePriorityInteractive.GetDetectPriority() > priorityInteractive.GetDetectPriority())
                    {
                        priorityInteractive = possiblePriorityInteractive;
                    }
                }

                _pos = priorityInteractive.SnapPosition(_pos);
                _object = priorityInteractive.gameObject;
                return true;
            }
        }

        // Checking for wall in a way of a cursor
        {
            Vector2 direction = _pos - (Vector2)transform.position;
            RaycastHit2D hit = Physics2D.Raycast(transform.position, direction, direction.magnitude, wallLayerMask);
            if (hit.collider != null && (_ignore == null || !IsDescendent(_ignore.transform, hit.transform)))
            {
                _object = hit.collider.gameObject;
                _pos = hit.point;
                return true;
            }
        }

        return false;
    }

    private static bool IsDescendent(Transform _parent, Transform _descendent)
    {
        Transform transformIt = _descendent;
        while (transformIt != null)
        {
            if (transformIt == _parent)
            {
                return true;
            }

            transformIt = transformIt.parent;
        }

        return false;
    }

    private static NetInteractive FindTarget(GameObject _object)
    {
        Transform objectIt = _object.transform;
        while (objectIt != null)
        {
            NetInteractive target = objectIt.GetComponent<NetInteractive>();
            if (target != null && target.IsValid())
            {
                return target;
            }

            objectIt = objectIt.parent;
        }

        return null;
    }

    private bool CheckVisionForTarget(NetInteractive _target, Vector2 _pos)
    {
        Vector2 direction = _pos - (Vector2)transform.position;
        RaycastHit2D[] hits = Physics2D.RaycastAll(transform.position, direction, direction.magnitude, (1 << LayerMask.NameToLayer("Wall")));
        foreach (RaycastHit2D hit in hits)
        {
            if (hit.collider.gameObject != null && FindTarget(hit.collider.gameObject) != _target)
            {
                return false;
            }
        }

        return true;
    }

    private bool CheckVisionBetweenTargets(GameObject _leftObject, Vector2 _leftPos, GameObject _rightObject, Vector2 _rightPos)
    {
        Vector2 direction = _rightPos - _leftPos;
        RaycastHit2D[] hits = Physics2D.RaycastAll(_leftPos, direction, direction.magnitude, (1 << LayerMask.NameToLayer("Wall")));
        foreach (RaycastHit2D hit in hits)
        {
            if (hit.collider == null)
            {
                continue;
            }

            if (IsDescendent(_leftObject.transform, hit.transform) || IsDescendent(_rightObject.transform, hit.transform))
            {
                continue;
            }

            return false;
        }

        return true;
    }

    private void UpdateAim(Vector2 _pos)
    {
        m_aimedPos = _pos;
        GameObject aimedObject = null;
        ResolveTraget(ref m_aimedPos, ref aimedObject);

        m_aimedInteractive = aimedObject != null
            ? aimedObject.GetComponent<NetInteractive>()
            : null;
    }

    private void OnShootingPerformed(InputAction.CallbackContext _context)
    {
        if (ItemsRegistry<SpiderWeb>.GetInstance().GetActiveItemsCount() >= m_MaxWebCount)
        {
            Debug.Log("Can't shoot web, because where is too much of it in the level!");
            return;
        }

        Vector2 aimedPosition = Camera.main.ScreenToWorldPoint(Input.mousePosition);
        if (Vector2.Distance(transform.position, aimedPosition) > m_MaxShotDistance)
        {
            Debug.Log("Can't shoot web, because aiming is too far!");
            return;
        }

        GameObject aimedObject = null;
        bool hasHit = ResolveTraget(ref aimedPosition, ref aimedObject, m_firstPendingObject);
        if (!hasHit)
        {
            if(GlobalAudioPlayer.GetInstance())
            {
                GlobalAudioPlayer.GetInstance().PlayClip(GlobalAudioPlayer.GetInstance().WebMiss);
            }
            Debug.Log("Failed to shoot net, because has no hit.");
            Debug.DrawLine(transform.position, aimedPosition, Color.red, 1.0f);
            return;
        }

        if (m_firstPendingObject == null)
        {
            m_firstPendingObject = aimedObject;
            m_firstPendingPoint = aimedPosition;

            // New code with hold web mechanic
            if(m_ImaginableWebHolder != null)
            {
                Destroy(m_ImaginableWebHolder.gameObject); 
            }
            m_ImaginableWebHolder = Object.Instantiate<ImaginableNetVisual>(m_ImaginableWebPrefab);
            m_ImaginableWebHolder.SetFirstPoint(m_firstPendingPoint);
            // New code end
        }
        else
        {
            if (!CheckVisionBetweenTargets(m_firstPendingObject, m_firstPendingPoint, aimedObject, aimedPosition))
            {
                Debug.Log("Failed to shoot net, because visibility between two points is abstructed.");
                Debug.DrawLine(transform.position, aimedPosition, Color.red, 1.0f);
                if (m_ImaginableWebHolder)
                {
                    Destroy(m_ImaginableWebHolder.gameObject);
                }

                return;
            }
            // New code with hold web mechanic
            if(m_ImaginableWebHolder)
            {
                Destroy(m_ImaginableWebHolder.gameObject);
            }
            SpiderWeb spawnedObject = Object.Instantiate<SpiderWeb>(m_SpiderWebPrefab); ;
            // New code end
            spawnedObject.AttachPoints(m_firstPendingObject, m_firstPendingPoint, aimedObject, aimedPosition);
            m_firstPendingObject = null;
            m_ImaginableWebHolder = null;
        }

        Debug.DrawLine(transform.position, aimedPosition, Color.green, 1.0f);
    }
    private void OnCancelPerformed(InputAction.CallbackContext _context)
    {

        if (m_firstPendingObject != null)
        {
            Debug.Log("Canceling net shooting.");
            m_firstPendingObject = null;

            if (m_ImaginableWebHolder != null)
            {
                Destroy(m_ImaginableWebHolder.gameObject);
            }
            return;
        }

        int interactiveLayerMask = 1 << LayerMask.NameToLayer("InteractiveSnap");
        Vector2 aimedPosition = Camera.main.ScreenToWorldPoint(Input.mousePosition);
        RaycastHit2D[] hits = Physics2D.RaycastAll(aimedPosition, aimedPosition, 0.0f, interactiveLayerMask);

        foreach (RaycastHit2D hit in hits)
        {
            if (hit.collider == null)
            {
                continue;
            }

            NetInteractive target = FindTarget(hit.collider.gameObject);
            if (target == null)
            {
                continue;
            }

            SpiderWeb web = target as SpiderWeb;
            if (web == null)
            {
                continue;
            }

            Debug.Log($"Removing target '{web.gameObject}'.");
            web.InvalidateWithDelay(0.0f, null);
        }
    }

    private void OnAimingPerformed(InputAction.CallbackContext _context)
    {
        UpdateAim(_context.ReadValue<Vector2>());
    }

    private void OnDrawGizmos()
    {
        Gizmos.color = m_aimedInteractive != null ? Color.green : Color.white;
        if (m_aimedInteractive != null )
        {
            Gizmos.color = Color.green;
            Gizmos.DrawWireSphere(m_aimedPos, 1.0f + Mathf.Cos(Time.time * 4.0f) * 0.3f);
        }
        else
        {
            Gizmos.color = Color.white;
            Gizmos.DrawWireSphere(m_aimedPos, 1.0f);
        }

        if (m_firstPendingObject != null)
        {
            Gizmos.color = Color.yellow;
            Gizmos.DrawWireSphere(m_firstPendingPoint, 1.0f + Mathf.Cos(Time.time * 1.5f) * 0.1f);
        }
    }

    private void Awake()
    {
        m_inputActions = new PlayerInput();
    }
    private void Start()
    {
        m_MainSight = Instantiate(m_SightPrefab);
        m_SecondarySight = Instantiate(m_SightPrefab);
        m_SecondarySight.Hide();
    }

    private void Update()
    {
        UpdateAim(Camera.main.ScreenToWorldPoint(Input.mousePosition));
        if (m_ImaginableWebHolder != null)
        {
            m_ImaginableWebHolder.SetSecondPoint(m_aimedPos);
        }

        if (m_aimedInteractive != null)
        {
            m_MainSight.SetColor(Color.green);
            m_MainSight.transform.position = m_aimedPos;
            float scale = 1.0f + Mathf.Cos(Time.time * 4.0f) * 0.3f;
            m_MainSight.transform.localScale = new Vector3(scale, scale, 1);
        }
        else
        {
            m_MainSight.SetColor(Color.white);
            m_MainSight.transform.position = m_aimedPos;
            m_MainSight.transform.localScale = new Vector3(1, 1, 1);
        }

        if (m_firstPendingObject != null)
        {
            m_SecondarySight.Show();
            m_SecondarySight.SetColor(Color.yellow);
            m_SecondarySight.transform.position = m_firstPendingPoint;
            float scale = 1.0f + Mathf.Cos(Time.time * 1.5f) * 0.1f;
            m_SecondarySight.transform.localScale = new Vector3(scale, scale, 1);
        }
        else
        {
            m_SecondarySight.Hide();
        }


        if (m_WebLeftText != null)
        {
            int webLeft = m_MaxWebCount - ItemsRegistry<SpiderWeb>.GetInstance().GetActiveItemsCount();
            m_WebLeftText.text = $"Web Available: {webLeft}";
        }
    }

    private void OnEnable()
    {
        m_inputActions.Shooting.Trigger.performed += OnShootingPerformed;
        m_inputActions.Shooting.Cancel.performed += OnCancelPerformed;
        m_inputActions.Enable();
    }

    private void OnDisable()
    {
        m_inputActions.Disable();
        m_inputActions.Shooting.Trigger.performed -= OnShootingPerformed;
        m_inputActions.Shooting.Cancel.performed -= OnCancelPerformed;
    }
}
