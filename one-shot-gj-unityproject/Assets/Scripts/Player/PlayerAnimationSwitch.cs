﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerAnimationSwitch : MonoBehaviour
{
    [SerializeField]
    Animator m_Animator;
    [SerializeField]
    Transform m_Rotation;

    GroundMovement m_GroundMovement;
    private void Awake()
    {
        m_GroundMovement = GetComponent<GroundMovement>();
    }
    private void Start()
    {
        m_Animator.SetBool("Idle", true);
    }
    private void Update()
    {
        Vector2 movement = m_Rotation.rotation * m_GroundMovement.GetMovementDirection();
        if (movement == Vector2.zero)
        {
            m_Animator.SetBool("Idle",true);
            m_Animator.SetBool("Forward", false);
            m_Animator.SetBool("Backward", false);
            m_Animator.SetBool("Left", false);
            m_Animator.SetBool("Right", false);
        } 
        else
        {
            m_Animator.SetBool("Idle", false);
            if(Mathf.Abs(movement.y) > Mathf.Abs(movement.x))
            {
                m_Animator.SetBool("Forward", movement.y < 0);
                m_Animator.SetBool("Backward", movement.y > 0);
                m_Animator.SetBool("Left", false);
                m_Animator.SetBool("Right", false);
            }
            else
            {
                m_Animator.SetBool("Forward", false);
                m_Animator.SetBool("Backward", false);
                m_Animator.SetBool("Left", movement.x < 0);
                m_Animator.SetBool("Right", movement.x > 0);
            }
        }
    }
}
