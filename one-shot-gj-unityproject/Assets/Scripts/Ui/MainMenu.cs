using Data;
using Global;
using TMPro;
using UnityEngine;
using UnityEngine.UI;

namespace Ui
{
    public class MainMenu : Screen
    {
        public override ScreenType ScreenType => ScreenType.MainMenu;

        [SerializeField] private Button _playButton;
        [SerializeField] private Button _howToPlayButton;
        [SerializeField] private Button _quit;

        [SerializeField] private TextMeshProUGUI _highScoreText;

        private void Awake()
        {
            _playButton.onClick.AddListener(HandlePlayCommand);
            _quit.onClick.AddListener(HandleQuitCommand);
            _howToPlayButton.onClick.AddListener(HandleHowToPlayCommand);
        }

        private void OnDestroy()
        {
            _playButton.onClick.RemoveListener(HandlePlayCommand);
            _quit.onClick.RemoveListener(HandleQuitCommand);
            _howToPlayButton.onClick.RemoveListener(HandleHowToPlayCommand);
        }

        private void Start()
        {
            _highScoreText.text = PersistentData.Instance.MaxScore.Value.ToString();
        }

        private void HandlePlayCommand()
        {
            Hide();
            GameModeSwitcher.Instance.LoadGameLevel();
        }

        private void HandleQuitCommand()
        {
            Application.Quit();
#if UNITY_EDITOR
            UnityEditor.EditorApplication.isPlaying = false;
#endif
        }

        private void HandleHowToPlayCommand()
        {
            Hide();
            UiManager.Instance.GetScreen<HowToPlay>(ScreenType.HowToPlay).Show();
        }
    }
}