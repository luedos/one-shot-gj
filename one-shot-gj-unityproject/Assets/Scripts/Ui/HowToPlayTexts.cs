using System.Collections.Generic;
using UnityEngine;

namespace Ui
{
    [CreateAssetMenu]
    public class HowToPlayTexts : ScriptableObject
    {
        [field: TextAreaAttribute(3, 15)]
        [field: SerializeField]
        public List<string> Texts { get; private set; }
    }
}