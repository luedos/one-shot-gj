namespace Ui
{
    public enum ScreenType
    {
        MainMenu,
        GameOver,
        HowToPlay,
        LoadingScreen,
    }
}