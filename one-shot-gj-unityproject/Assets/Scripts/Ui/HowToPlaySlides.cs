using System;
using System.Collections.Generic;
using OneShot.Net;
using TMPro;
using UnityEngine;
using UnityEngine.UI;

namespace Ui
{
    public class HowToPlaySlides : MonoBehaviour
    {
        [SerializeField] private HowToPlayTexts _texts;
        [SerializeField] private GameObjectFactory<TextMeshProUGUI> _textsFactory;
        [SerializeField] private Button _next;
        [SerializeField] private Button _previous;

        private List<TextMeshProUGUI> _textGuis = new List<TextMeshProUGUI>();
        private int _activeTextIndex;

        private void Awake()
        {
            _next.onClick.AddListener(SwitchToNext);
            _previous.onClick.AddListener(SwitchToPrevious);
            SpawnTexts();
        }

        private void OnDestroy()
        {
            _next.onClick.RemoveListener(SwitchToNext);
            _previous.onClick.RemoveListener(SwitchToPrevious);
        }

        private void OnEnable()
        {
            _activeTextIndex = 0;
            UpdateActiveText();
            UpdateButtonsIsActive();
        }

        private void SpawnTexts()
        {
            for (var i = 0; i < _texts.Texts.Count; i++)
            {
                string text = _texts.Texts[i];
                TextMeshProUGUI textGui = _textsFactory.Create();
                textGui.text = text;
                textGui.gameObject.SetActive(i == 0);
                _textGuis.Add(textGui);
            }
        }

        private void UpdateActiveText()
        {
            for (var i = 0; i < _texts.Texts.Count; i++)
            {
                TextMeshProUGUI textGui = _textGuis[i];
                textGui.gameObject.SetActive(i == _activeTextIndex);
            }
        }

        private void SwitchToNext()
        {
            _activeTextIndex = Mathf.Min(_activeTextIndex + 1, _textGuis.Count - 1);
            UpdateActiveText();
            UpdateButtonsIsActive();
        }

        private void SwitchToPrevious()
        {
            _activeTextIndex = Mathf.Max(_activeTextIndex - 1, 0);
            UpdateActiveText();
            UpdateButtonsIsActive();
        }

        private void UpdateButtonsIsActive()
        {
            _next.interactable = _activeTextIndex != _textGuis.Count - 1;
            _previous.interactable = _activeTextIndex != 0;
        }
    }
}