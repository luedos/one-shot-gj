using System.Collections.Generic;
using UnityEngine;

namespace Ui
{
    public class UiManager : MonoBehaviour
    {
        public static UiManager Instance { get; private set; }

        [SerializeReference] private List<Screen> _screens;

        private Dictionary<ScreenType, Screen> _screensLookup;

        private void Awake()
        {
            _screensLookup = new Dictionary<ScreenType, Screen>(_screens.Count);
            Instance = this;
            DontDestroyOnLoad(this);

            for (int i = 0; i < _screens.Count; i++)
            {
                Screen screen = _screens[i];
                screen.Hide();
                _screensLookup.Add(screen.ScreenType, screen);
            }
        }

        public T GetScreen<T>(ScreenType type) where T : Screen
        {
            if (!_screensLookup.TryGetValue(type, out Screen abstractScreen))
            {
                Debug.LogError($"ScreenType {type} not found");
                return null;
            }

            if (abstractScreen is T screen)
            {
                return screen;
            }

            Debug.LogError($"Requested screen of wrong type {type}. Actual type is {abstractScreen.ScreenType}");
            return null;
        }

        public void HideAll()
        {
            for (var i = 0; i < _screens.Count; i++)
            {
                _screens[i].Hide();
            }
        }
    }
}