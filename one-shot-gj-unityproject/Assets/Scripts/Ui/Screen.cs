using UnityEngine;

namespace Ui
{
    public abstract class Screen : MonoBehaviour
    {
        public abstract ScreenType ScreenType { get; }

        public void Show()
        {
            gameObject.SetActive(true);
        }

        public void Hide()
        {
            gameObject.SetActive(false);
        }
    }
}