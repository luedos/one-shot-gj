using Data;
using Global;
using Scenes;
using TMPro;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

namespace Ui
{
    public class GameOver : Screen
    {
        public override ScreenType ScreenType => ScreenType.GameOver;
        
        [SerializeField] private Button _tryAgainButton;
        [SerializeField] private Button _mainMenuButton;

        [SerializeField] private TextMeshProUGUI _highScoreText;
        [SerializeField] private TextMeshProUGUI _yourScoreText;
        [SerializeField] private TextMeshProUGUI _lastDamageCauseText;

        private void Awake()
        {
            _tryAgainButton.onClick.AddListener(HandleTryAgainCommand);
            _mainMenuButton.onClick.AddListener(HandleMainMenuCommand);
        }

        private void OnDestroy()
        {
            _tryAgainButton.onClick.RemoveListener(HandleTryAgainCommand);
            _mainMenuButton.onClick.RemoveListener(HandleMainMenuCommand);
        }
        
        private void Start()
        {
            _highScoreText.text = PersistentData.Instance.MaxScore.Value.ToString();
            _yourScoreText.text = Gamemode.Instance.GetCurrentScore().ToString();
            _lastDamageCauseText.text = $"Last damage by:\n{Gamemode.Instance.GetLastDamageCause()}";
        }

        private void HandleMainMenuCommand()
        {
            SceneManager.LoadScene(SceneNames.Main);
        }

        private void HandleTryAgainCommand()
        {
            Hide();
            GameModeSwitcher.Instance.LoadGameLevel();
        }
    }
}