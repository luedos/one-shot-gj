using System;
using UnityEngine;
using UnityEngine.UI;

namespace Ui
{
    public class HowToPlay : Screen
    {
        public override ScreenType ScreenType => ScreenType.HowToPlay;

        [SerializeField] private Button _toMainMenuButton;

        private void Awake()
        {
            _toMainMenuButton.onClick.AddListener(HandleToMainMenuCommand);
        }

        private void OnDestroy()
        {
            _toMainMenuButton.onClick.RemoveListener(HandleToMainMenuCommand);
        }

        private void HandleToMainMenuCommand()
        {
            Hide();
            UiManager.Instance.GetScreen<MainMenu>(ScreenType.MainMenu).Show();
        }
    }
}