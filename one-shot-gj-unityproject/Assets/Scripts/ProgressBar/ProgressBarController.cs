using UnityEngine;

public class ProgressBarController : MonoBehaviour
{
    Material m_Material;
    SpriteRenderer m_SpriteRenderer;
    float m_Begin;
    float m_End;
    private void Awake()
    {
        m_SpriteRenderer = GetComponent<SpriteRenderer>();
        m_Material = m_SpriteRenderer.material;
        m_SpriteRenderer.enabled = false;
    }
    public void SetProgressColor(Color color)
    {
        m_Material.SetColor("_ProgressColor", color);
    }
    public void Show()
    {
        m_SpriteRenderer.enabled = true;
        m_Material.SetFloat("_Progress", 0);

    }
    public void Hide()
    {
        m_SpriteRenderer.enabled = false;
    }
    public void SetupRange(float begin,float end)
    {
        m_Begin = begin;
        m_End = end;
    }
    public void SetState(float value)
    {
        m_Material.SetFloat("_Progress", (value - m_Begin) / (m_End - m_Begin));
    }

}
