using OneShot.Net;
using System;
using System.Collections;
using System.Collections.Generic;
using Unity.VisualScripting;
using UnityEngine;

[Serializable]
public class NetClaster
{
    [SerializeField] private List<NetInteractive> m_interactives = new List<NetInteractive>();

    public NetClaster()
    {
    }
    
    public NetClaster(IEnumerable<NetInteractive> _interactives)
    {
        m_interactives.AddRange(_interactives);
        foreach (NetInteractive interactive in m_interactives)
        {
            interactive.SimpleResetClaster(this);
        }
    }

    // Transfers all objects and interactives to the other claster.
    public void TransferState(NetClaster _other)
    {
        _other.m_interactives.AddRange(m_interactives);

        foreach (NetInteractive interactive in m_interactives)
        {
            interactive.SimpleResetClaster(_other);
        }

        m_interactives.Clear();
    }

    public bool HasInteractive(NetInteractive _interactive)
    {
        return m_interactives.Contains(_interactive);
    }

    public void RemoveInteractive(NetInteractive _interactive)
    {
        m_interactives.Remove(_interactive);
        if (m_interactives.Count == 0)
        {
            NetClasterManager.GetInstance().RemoveClaster(this);
        }
    }

    public IEnumerable<NetInteractive> GetInteractives()
    {
        return m_interactives;
    }
}
