using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class NetClasterManager
{
    private List<NetClaster> m_clasters = new List<NetClaster>();

    static private NetClasterManager m_instance = null;
    static public NetClasterManager GetInstance()
    {
        if (m_instance == null)
        {
            m_instance = new NetClasterManager();
        }

        return m_instance;
    }

    public NetClaster CreateClaster(IEnumerable<NetInteractive> _interactives)
    {
        NetClaster claster = new NetClaster(_interactives);

        m_clasters.Add(claster);
        return claster;
    }

    public void RemoveClaster(NetClaster _claster)
    {
        m_clasters.Remove(_claster);
    }
}
