using System.Collections;
using System.Collections.Generic;
using System.Linq;
using Unity.VisualScripting;
using UnityEngine;
using UnityEngine.Rendering.VirtualTexturing;

public class NetVisual : MonoBehaviour
{
    [SerializeField] private Transform m_LeftPoint = null;
    [SerializeField] private Transform m_RightPoint = null;
    [SerializeField] private LineRenderer m_Line = null;

    [SerializeField] private GameObject m_WebEndPrefab = null;
    [SerializeField] private ShortableWebVisual m_OnWebDestroyPrefab = null;

    [SerializeField] private Color m_InvalidatedColor = Color.white;
    [SerializeField] private Color m_VirusColor = Color.white;
    [SerializeField] private Color m_BadVirusColor = Color.white;
    [SerializeField] private ParticleSystem m_OnSpawnParticleSystem = null;

    private ParticleSystem m_LeftParticleSystem = null;
    private ParticleSystem m_RightParticleSystem = null;
    private Color m_defaultColor = Color.white;

    private SpiderWeb m_SpiderWeb = null;
    bool m_IsQuitting = false;
    private void Awake()
    {
        m_SpiderWeb = GetComponent<SpiderWeb>();
    }
    private void Start()
    {
        m_defaultColor = m_Line.startColor;

        SetupParticles();
        GameObject left = Instantiate(m_WebEndPrefab, m_LeftPoint.position,Quaternion.identity,transform);
        GameObject right = Instantiate(m_WebEndPrefab, m_RightPoint.position, Quaternion.identity, transform);

        Vector2 dir = (left.transform.position - right.transform.position).normalized;

        //left.transform.Translate(100 * dir); ???
        //left.transform.Translate(-100 * dir);
        left.transform.localScale = new Vector3(-1,2,1);
        right.transform.localScale = new Vector3(1, 2, 1);
        float angle = Vector2.Angle(left.transform.position - right.transform.position, Vector2.right);
        left.transform.localRotation = Quaternion.Euler(0, 0, angle - 180);
        right.transform.localRotation = Quaternion.Euler(0, 0, angle - 180);
    }
    void SetupParticles()
    {
        m_LeftParticleSystem = Instantiate(m_OnSpawnParticleSystem);
        m_RightParticleSystem = Instantiate(m_OnSpawnParticleSystem);

        m_LeftParticleSystem.transform.position = m_LeftPoint.position;
        m_RightParticleSystem.transform.position = m_RightPoint.position;
        float angle1 = Vector2.Angle(Vector2.left, m_LeftPoint.position - m_RightPoint.position);
        m_LeftParticleSystem.transform.rotation = Quaternion.Euler(
            m_LeftParticleSystem.transform.rotation.eulerAngles.x + angle1 + 180,
            m_LeftParticleSystem.transform.rotation.eulerAngles.y,
            m_LeftParticleSystem.transform.rotation.eulerAngles.z
            );

        m_RightParticleSystem.transform.rotation = Quaternion.Euler(
            m_RightParticleSystem.transform.rotation.eulerAngles.x - angle1,
            m_RightParticleSystem.transform.rotation.eulerAngles.y,
            m_RightParticleSystem.transform.rotation.eulerAngles.z
            );
    }
    ParticleSystem TryDestroyPartices(ParticleSystem system)
    {
        ParticleSystem result = system;
        if (system && !system.isEmitting)
        {
            Destroy(system.gameObject);
            result = null;
        }

        return result;
    }
    // Update is called once per frame
    void Update()
    {
        SetLinePositions(m_Line, m_LeftPoint.position, m_RightPoint.position);

        Color color = m_defaultColor;

        if (m_SpiderWeb.IsInvalidated())
        {
            color = m_InvalidatedColor;
        }
        else if (m_SpiderWeb.IsConnectedToBadVirus())
        {
            color = m_BadVirusColor;
        }
        else if (m_SpiderWeb.IsConnectedToVirus())
        {
            color = m_VirusColor;
        }

        m_Line.startColor = color;
        m_Line.endColor = color;

        m_LeftParticleSystem = TryDestroyPartices(m_LeftParticleSystem);
        m_RightParticleSystem = TryDestroyPartices(m_RightParticleSystem);
    }
    void OnApplicationQuit()
    {
        m_IsQuitting = true;
    }
    private void OnDestroy()
    {
        TryDestroyPartices(m_LeftParticleSystem);
        TryDestroyPartices(m_RightParticleSystem);

        if (m_IsQuitting)
            return;

        if (m_SpiderWeb.GetInvalidatedPoint() == null)
        {
            ShortableWebVisual left = Instantiate(m_OnWebDestroyPrefab);
            ShortableWebVisual right = Instantiate(m_OnWebDestroyPrefab);

            var posLeft = m_LeftPoint.position;
            var posRight = m_RightPoint.position;

            left.SetStaticPoint(posLeft);
            right.SetStaticPoint(posRight);
            float d = (posLeft - posRight).magnitude;
            var middle = UnityEngine.Vector3.MoveTowards(posLeft, posRight, d / 2);
            left.SetDynamicPoint(middle);
            right.SetDynamicPoint(middle);
        }
        else
        {
            ShortableWebVisual web = Instantiate(m_OnWebDestroyPrefab);
            var points = m_SpiderWeb.GetPoints().ToArray();
            var left = points[0];
            var right = points[1];
            if(left == m_SpiderWeb.GetInvalidatedPoint())
            {
                web.SetDynamicPoint(m_LeftPoint.position);
                web.SetStaticPoint(m_RightPoint.position);
            }
            else if (right == m_SpiderWeb.GetInvalidatedPoint())
            {
                web.SetDynamicPoint(m_RightPoint.position);
                web.SetStaticPoint(m_LeftPoint.position);
            }
            else
            {
                //Should never appear as far as I understood but still 
                Destroy(web.gameObject);
            }
        }
    }

    private static void SetLinePositions(LineRenderer _render, Vector3 _start, Vector3 _end)
    {
        Vector3 step = (_end - _start) / (_render.positionCount - 1);
        for (int pointIndex = 0; pointIndex < _render.positionCount; ++pointIndex)
        {
            _render.SetPosition(pointIndex, _start + step * pointIndex);
        }
    }
}
