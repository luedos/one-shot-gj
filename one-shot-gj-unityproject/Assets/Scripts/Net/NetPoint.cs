using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class NetPoint : MonoBehaviour
{
    private NetObject m_net = null;
    private GameObject m_attachedObject = null;
    private NetInteractive m_attachedInteractive = null;

    public NetInteractive GetAttachedInteractive()
    {
        return m_attachedInteractive;
    }

    public NetObject GetParentNetObject()
    {
        return m_net;
    }

    public void SetParentNetObject(NetObject _object)
    {
        m_net = _object;
    }

    public void AttachToObject(GameObject _object, Vector3 _position)
    {
        NetInteractive newInteractive = _object.GetComponent<NetInteractive>();
        if (newInteractive != m_attachedInteractive)
        {
            if (m_attachedInteractive != null)
            {
                m_attachedInteractive.SimpleRemovePoint(this);
            }

            m_attachedInteractive = newInteractive;
            if (m_attachedInteractive != null)
            {
                m_attachedInteractive.SimpleAddPoint(this);
            }
        }

        m_attachedObject = _object;
        transform.position = _position;
    }

    public void DeattachFromObject()
    {
        if (m_attachedInteractive != null)
        {
            m_attachedInteractive.SimpleRemovePoint(this);
        }

        m_attachedInteractive = null;
        m_attachedObject = null;
    }

    public bool IsAttached()
    {
        return m_attachedObject != null && m_attachedObject.activeSelf && m_attachedObject.activeInHierarchy;
    }

    public bool IsAttachedTo(GameObject _object)
    {
        return m_attachedObject == _object && m_attachedObject.activeSelf && m_attachedObject.activeInHierarchy;
    }
}
