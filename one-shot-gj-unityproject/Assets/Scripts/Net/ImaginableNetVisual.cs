using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ImaginableNetVisual : MonoBehaviour
{
    LineRenderer m_LineRenderer;
    Vector3 m_firstPoint = Vector3.zero;

    private void Awake()
    {
        m_LineRenderer = GetComponent<LineRenderer>();
    }
    public void SetFirstPoint(Vector2 position)
    {
        m_firstPoint = position;
        SetLinePositions(m_LineRenderer, position, position);
    }
    public void SetSecondPoint(Vector2 position)
    {
        SetLinePositions(m_LineRenderer, position, m_firstPoint);
    }

    private static void SetLinePositions(LineRenderer _render, Vector3 _start, Vector3 _end)
    {
        Vector3 step = (_end - _start) / (_render.positionCount - 1);
        for (int pointIndex = 0; pointIndex < _render.positionCount; ++pointIndex)
        {
            _render.SetPosition(pointIndex, _start + step * pointIndex);
        }
    }
}
