using System.Collections;
using System.Collections.Generic;
using Unity.VisualScripting;
using UnityEngine;

public class SpiderWeb : NetObject, IRegistryEntry
{
    [SerializeField] private NetPoint m_LeftPoint = null;
    [SerializeField] private NetPoint m_RightPoint = null;
    [SerializeField] private float m_RaycastHitTolerance = 0.1f;

    private bool m_ignoreCollisionBreaking = true;
    private bool m_connectedToVirus = false;
    private bool m_connectedToBadVirus = false;

    public NetPoint GetInvalidatedPoint() { return m_InvalidatedPoint; }
    public override IEnumerable<NetPoint> GetPoints()
    {
        return new[] { m_LeftPoint, m_RightPoint };
    }

    public NetPoint GetLeftPoint()
    {
        return m_LeftPoint;
    }

    public NetPoint GetRightPoint()
    {
        return m_RightPoint;
    }

    public bool IsAttachedToObject(GameObject _object)
    {
        return m_LeftPoint.IsAttachedTo(_object) || m_RightPoint.IsAttachedTo(_object);
    }

    public void AttachPoints(GameObject _leftObject, Vector3 _leftPosition, GameObject _rightObject, Vector3 _rightPosition)
    {
        m_LeftPoint.DeattachFromObject();
        m_RightPoint.DeattachFromObject();
        ClearAttachmentsSilent();

        m_LeftPoint.AttachToObject(_leftObject, _leftPosition);
        m_RightPoint.AttachToObject(_rightObject, _rightPosition);

        RecreateClaster();

        ResetInvalidationTimer();
        m_ignoreCollisionBreaking = false;
    }

    public void SetIgnoreCollisionBreaking(bool _ignoreCollisionBreaking)
    {
        m_ignoreCollisionBreaking = _ignoreCollisionBreaking;
    }

    public override bool IsValid()
    {
        return base.IsValid() && m_RightPoint.IsAttached() && m_LeftPoint.IsAttached();
    }

    public bool IsConnectedToVirus()
    {
        return m_connectedToVirus;
    }

    public bool IsConnectedToBadVirus()
    {
        return m_connectedToBadVirus;
    }

    private void Awake()
    {
        m_LeftPoint.SetParentNetObject(this);
        m_RightPoint.SetParentNetObject(this);
    }
    private void Start()
    {
        if(GlobalAudioPlayer.GetInstance())
        {
            GlobalAudioPlayer.GetInstance().PlayClip(GlobalAudioPlayer.GetInstance().SpiderWebSpawn);
        }
    }

    private void FixedUpdate()
    {
        if (!m_ignoreCollisionBreaking && !IsInvalidated())
        {
            int wallLayerMask = 1 << LayerMask.NameToLayer("Wall");
            Vector2 leftPoint = m_LeftPoint.transform.position;
            Vector2 rightPoint = m_RightPoint.transform.position;
            Vector2 direction = rightPoint - leftPoint;

            RaycastHit2D[] hits = Physics2D.RaycastAll(leftPoint + direction.normalized * m_RaycastHitTolerance, direction, direction.magnitude - m_RaycastHitTolerance * 2, wallLayerMask);
            foreach (RaycastHit2D hit in hits)
            {
                if (hit.collider == null)
                {
                    continue;
                }

                NetInteractive possibleInteractive = FindInteractive(hit.collider.gameObject);
                if (possibleInteractive != null && IsChildInteractive(possibleInteractive))
                {
                    continue;
                }

                Destroy(gameObject);
                return;
            }
        }
    }

    protected new void OnEnable()
    {
        base.OnEnable();
        ItemsRegistry<SpiderWeb>.GetInstance().RegisterItem(this);
    }

    protected new void OnDisable()
    {
        ItemsRegistry<SpiderWeb>.GetInstance().UnregisterItem(this);
        base.OnDisable();
    }

    protected new void Update()
    {
        base.Update();

        UpdateConnectedToVirus();

        if (m_IsInvalidatable && base.IsValid())
        {
            if (!m_LeftPoint.IsAttached() || !m_RightPoint.IsAttached())
            {
                Invalidate();
            }
        }
    }

    private void UpdateConnectedToVirus()
    {
        m_connectedToBadVirus = false;
        m_connectedToVirus = false;
        bool connectedToRegularComputer = false;

        NetClaster claster = GetClaster();
        if (claster == null)
        {
            return;
        }

        foreach (NetInteractive interactive in claster.GetInteractives())
        {
            Computer potentialComputer = interactive as Computer;
            if (potentialComputer != null)
            {
                if (potentialComputer.IsVirus())
                {
                    m_connectedToVirus = true;
                }
                else
                {
                    connectedToRegularComputer = true;
                }

                continue;
            }

            ConnectionNetItem potentialConnection = interactive as ConnectionNetItem;
            if (potentialConnection != null)
            {
                if (potentialConnection.IsVirus())
                {
                    m_connectedToVirus = true;
                }

                continue;
            }
        }

        m_connectedToBadVirus = m_connectedToVirus && connectedToRegularComputer;
    }

    private static NetInteractive FindInteractive(GameObject _object)
    {
        Transform objectIt = _object.transform;
        while (objectIt != null)
        {
            NetInteractive interactive = objectIt.GetComponent<NetInteractive>();
            if (interactive != null)
            {
                return interactive;
            }

            objectIt = objectIt.parent;
        }

        return null;
    }

    public string GetRegistryName()
    {
        return "";
    }
    private void OnDestroy()
    {
        if(GlobalAudioPlayer.GetInstance())
        {
            GlobalAudioPlayer.GetInstance().PlayClip(GlobalAudioPlayer.GetInstance().SpiderWebDestroy);

        }

    }
}
