using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class NetObjectCollision : MonoBehaviour
{
    [SerializeField] private Transform m_LeftPoint = null;
    [SerializeField] private Transform m_RightPoint = null;
    [SerializeField] private CapsuleCollider2D m_Collider = null;

    // Update is called once per frame
    void Update()
    {
        Vector2 direction = m_RightPoint.position - m_LeftPoint.position;
        Vector2 size = m_Collider.size;
        size.y = direction.magnitude + size.x;
        m_Collider.size = size;

        transform.position = (m_LeftPoint.position + m_RightPoint.position) / 2;
        transform.rotation = Quaternion.LookRotation(Vector3.forward, direction);
    }
}
