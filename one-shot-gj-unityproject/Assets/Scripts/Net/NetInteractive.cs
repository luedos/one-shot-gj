using System;
using System.Collections;
using System.Collections.Generic;
using Unity.VisualScripting;
using UnityEngine;
using UnityEngine.Events;

public class NetInteractive : MonoBehaviour
{
    [SerializeField] private Transform m_leftTransform = null;
    [SerializeField] private Transform m_rightTransform = null;
    [SerializeField] private UnityEvent<NetClaster> m_OnClasterChanged = null;
    [SerializeField] private int m_DetectPriority = 10;
    [SerializeField][Tooltip("The delay provided for every attached NetObject in the Invalidate call.")] private float m_InvalidateDelay = 0.1f;

    private NetClaster m_claster = null;
    private List<NetPoint> m_attachedPoints = new List<NetPoint>();
    private bool m_enabled = false;

    // Returns position on the object closest to the desired position.
    public Vector2 SnapPosition(Vector2 _position)
    {
        Vector2 leftPoint = m_leftTransform.position;
        Vector2 rightPoint = m_rightTransform.position;

        if (leftPoint == rightPoint)
        {
            return leftPoint;
        }

        Vector2 lineDirection = rightPoint - leftPoint;
        Vector2 pointDirection = _position - leftPoint;

        float dot1 = Vector2.Dot(pointDirection, lineDirection);
        float dot2 = Vector2.Dot(lineDirection, lineDirection);

        if (dot1 <= 0.0f)
        {
            return leftPoint;
        }
        else if (dot2 <= dot1)
        {
            return rightPoint;
        }
        else
        {
            return leftPoint + lineDirection * (dot1 / dot2);
        }
    }

    public Vector2 SnapDirection(Vector2 _start, Vector2 _direction)
    {
        Vector2 selfLeftPoint = m_leftTransform.position;
        Vector2 selfRightPoint = m_rightTransform.position;

        if (selfLeftPoint == selfRightPoint)
        {
            return selfLeftPoint;
        }

        Vector2 otherLeftPoint = _start;
        Vector2 otherRightPoint = _start + _direction;

        float a1 = selfRightPoint.y - selfLeftPoint.y;
        float b1 = selfLeftPoint.x - selfRightPoint.x;
        float c1 = a1 * selfLeftPoint.x + b1 * selfLeftPoint.y;

        float a2 = otherRightPoint.y - otherLeftPoint.y;
        float b2 = otherLeftPoint.x - otherRightPoint.x;
        float c2 = a2 * otherLeftPoint.x + b2 * otherLeftPoint.y;

        float determinant = a1 * b2 - a2 * b1;
        if (determinant == 0)
        {
            return (selfRightPoint - _start).magnitude < (selfLeftPoint - _start).magnitude
                ? selfRightPoint
                : selfLeftPoint;
        }

        Vector2 intersection = new Vector2(
            (b2 * c1 - b1 * c2) / determinant,
            (a1 * c2 - a2 * c1) / determinant);

        if (Vector2.Dot(intersection - selfLeftPoint, intersection - selfRightPoint) > 0.0f)
        {
            return (selfRightPoint - intersection).magnitude < (selfLeftPoint - intersection).magnitude
                ? selfRightPoint
                : selfLeftPoint;
        }

        return intersection;
    }

    public void SimpleAddPoint(NetPoint _point)
    {
        if (!m_attachedPoints.Contains(_point))
        {
            m_attachedPoints.Add(_point);
        }
    }

    public void SimpleRemovePoint(NetPoint _point)
    {
        m_attachedPoints.Remove(_point);
    }

    public NetClaster GetClaster()
    {
        return m_claster;
    }

    public NetClaster RecreateClaster()
    {
        List<NetInteractive> interactives = new List<NetInteractive>();
        CollectInteractives(interactives);

        return NetClasterManager.GetInstance().CreateClaster(interactives);
    }

    public virtual bool IsValid()
    {
        return m_enabled;
    }

    virtual public void CollectInteractives(List<NetInteractive> _result)
    {
        if (_result.Contains(this))
        {
            return;
        }

        _result.Add(this);

        foreach (NetPoint point in m_attachedPoints)
        {
            NetObject parentObject = point.GetParentNetObject();
            if (parentObject != null)
            {
                parentObject.CollectInteractives(_result);
            }
        }
    }

    public void SimpleResetClaster(NetClaster _claster)
    {
        if (_claster != m_claster)
        {
            if (m_claster != null)
            {
                m_claster.RemoveInteractive(this);
            }

            m_claster = _claster;

            OnClasterChanged(m_claster);
            m_OnClasterChanged.Invoke(m_claster);
        }
    }

    public IEnumerable<NetPoint> GetAttachedPoints()
    {
        return m_attachedPoints;
    }

    public void ClearAttachments()
    {
        ClearAttachmentsSilent();
        if (IsValid())
        {
            RecreateClaster();
        }
    }

    public int GetDetectPriority()
    {
        return m_DetectPriority;
    }

    protected float GetExpectedInvalidationDelay()
    {
        return m_InvalidateDelay;
    }

    protected void ClearAttachmentsSilent()
    {
        if (m_claster != null)
        {
            m_claster.RemoveInteractive(this);
        }

        List<NetInteractive> oldInteractives = m_claster != null
            ? new List<NetInteractive>(m_claster.GetInteractives())
            : new List<NetInteractive>();

        SimpleClearAttachements(m_InvalidateDelay);

        while (oldInteractives.Count > 0)
        {
            NetInteractive interactive = oldInteractives[oldInteractives.Count - 1];
            oldInteractives.Remove(interactive);

            if (interactive.IsValid())
            {
                NetClaster newClaster = interactive.RecreateClaster();

                foreach (NetInteractive newClasterInteractives in newClaster.GetInteractives())
                {
                    oldInteractives.Remove(newClasterInteractives);
                }
            }
        }

        m_claster = null;
    }

    protected void SimpleClearAttachements(float _invalidationDelay)
    {
        List<NetPoint> attachedPoints = new List<NetPoint>(GetAttachedPoints());
        List<NetObject> objectsToClear = new List<NetObject>();
        foreach (NetPoint childPoint in attachedPoints)
        {
            NetObject childObject = childPoint.GetParentNetObject();
            if (childObject.m_claster != null)
            {
                childObject.m_claster.RemoveInteractive(childObject);
                childObject.m_claster = null;
            }
            if (childObject.IsValid() && childObject.IsInvalidatable())
            {
                childObject.InvalidateWithDelaySimple(_invalidationDelay, childPoint);
                objectsToClear.Add(childObject);
            }
        }

        foreach (NetObject childObject in objectsToClear)
        {
            childObject.SimpleClearAttachements(_invalidationDelay + childObject.m_InvalidateDelay);
        }
    }

    protected void OnEnable()
    {
        m_enabled = true;
    }

    protected void OnDisable()
    {
        m_enabled = false;
        ClearAttachments();
    }

    protected virtual void OnClasterChanged(NetClaster _newClaster)
    {
    }
}
