using System.Collections;
using System.Collections.Generic;
using UnityEditor;
using UnityEngine;
using UnityEngine.UIElements;

public abstract class NetObject : NetInteractive
{
    [SerializeField] protected bool m_IsInvalidatable = true;
    private float m_invalidationTime = -1.0f;
    protected NetPoint m_InvalidatedPoint = null;

    public abstract IEnumerable<NetPoint> GetPoints();

    public void ResetInvalidationTimer()
    {
        m_invalidationTime = -1.0f;
    }

    public bool IsChildInteractive(NetInteractive _child)
    {
        foreach (NetPoint point in GetPoints())
        {
            if (point.GetAttachedInteractive() == _child)
            {
                return true;
            }
        }

        return false;
    }

    public override void CollectInteractives(List<NetInteractive> _result)
    {
        if (_result.Contains(this))
        {
            return;
        }

        base.CollectInteractives(_result);

        foreach (NetPoint point in GetPoints())
        {
            NetInteractive childInteractive = point.GetAttachedInteractive();
            if (childInteractive != null)
            {
                childInteractive.CollectInteractives(_result);
            }
        }
    }

    public void ClearAttachmentsAndPoints()
    {
        ClearPointsSimple();
        ClearAttachments();
    }

    public void InvalidateWithDelay(float _delay, NetPoint point)
    {
        if (m_IsInvalidatable)
        {
            m_InvalidatedPoint = point;
            m_invalidationTime = _delay;
            
            ClearAttachmentsAndPoints();
        }
    }

    public void Invalidate()
    {
        InvalidateWithDelay(GetExpectedInvalidationDelay(), null);
    }

    public void InvalidateWithDelaySimple(float _delay, NetPoint point)
    {
        if (m_IsInvalidatable)
        {
            m_InvalidatedPoint = point;
            m_invalidationTime = _delay;
            ClearPointsSimple();
        }
    }

    public bool IsInvalidated()
    {
        return m_invalidationTime > 0.0f;
    }

    public bool IsInvalidatable()
    {
        return m_IsInvalidatable;
    }

    public override bool IsValid()
    {
        return base.IsValid() && !IsInvalidated();
    }

    protected void ClearPointsSimple()
    {
        foreach (NetPoint point in GetPoints())
        {
            point.DeattachFromObject();
        }
    }

    protected new void OnDisable()
    {
        ClearPointsSimple();
        base.OnDisable();
    }

    protected void Update()
    {
        if (m_invalidationTime >= 0.0f)
        {
            m_invalidationTime -= Time.deltaTime;
            if (m_invalidationTime <= 0.0f)
            {
                Destroy(gameObject);
                m_invalidationTime = 0.0f;
            }
        }
    }

}
