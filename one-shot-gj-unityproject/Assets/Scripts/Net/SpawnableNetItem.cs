using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SpawnableNetItem : NetObject
{
    [SerializeField] private NetPoint m_NetPointPrefab = null;

    private List<NetPoint> m_points = new List<NetPoint>();

    public override IEnumerable<NetPoint> GetPoints()
    {
        return m_points;
    }

    private void OnTriggerEnter2D(Collider2D _collision)
    {
        if (_collision.gameObject == null)
        {
            return;
        }

        SpiderWeb web = FindWeb(_collision.gameObject);
        if (web == null)
        {
            return;
        }

        if (web.IsAttachedToObject(gameObject))
        {
            return;
        }

        if (FindPointAttachedTo(web) != null)
        {
            return;
        }

        NetPoint point = GetEmptyPoint();
        point.AttachToObject(web.gameObject, web.SnapPosition(transform.position));
        RecreateClaster();
    }

    private void OnTriggerExit2D(Collider2D _collision)
    {
        if (_collision == null || _collision.gameObject == null)
        {
            return;
        }

        SpiderWeb web = FindWeb(_collision.gameObject);
        if (web == null)
        {
            return;
        }

        NetPoint point = FindPointAttachedTo(web);
        if (point == null)
        {
            return;
        }

        point.DeattachFromObject();
        NetClaster newClaster = web.RecreateClaster();

        if (GetClaster() != newClaster)
        {
            RecreateClaster();
        }
    }

    private SpiderWeb FindWeb(GameObject _object)
    {
        Transform transformIt = _object.transform;
        while (transformIt != null)
        {
            SpiderWeb web = transformIt.GetComponent<SpiderWeb>();
            if (web != null)
            {
                return web;
            }

            transformIt = transformIt.parent;
        }

        return null;
    }

    private NetPoint GetEmptyPoint()
    {
        foreach (NetPoint point in m_points)
        {
            if (!point.IsAttached())
            {
                return point;
            }
        }

        NetPoint newPoint = Object.Instantiate<NetPoint>(m_NetPointPrefab, transform);
        newPoint.SetParentNetObject(this);
        m_points.Add(newPoint);
        return newPoint;
    }

    private NetPoint FindPointAttachedTo(SpiderWeb _web)
    {
        foreach (NetPoint point in m_points)
        {
            if (point.GetAttachedInteractive() == _web)
            {
                return point;
            }
        }

        return null;
    }
}
