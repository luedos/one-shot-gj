using UnityEngine;

public class ShortableWebVisual : MonoBehaviour
{
    Vector3 m_StaticPoint;
    Vector3 m_DynamicPoint;
    LineRenderer m_LineRenderer;
    [SerializeField]
    float m_Speed = 8;
    // Start is called before the first frame update

    public void SetStaticPoint(Vector3 point) => m_StaticPoint = point;
    public void SetDynamicPoint(Vector3 point) => m_DynamicPoint = point;


    void Awake()
    {
        ///Vector3.MoveTowards
        m_LineRenderer = GetComponent<LineRenderer>();

    }

    // Update is called once per frame
    void Update()
    {
        m_DynamicPoint = Vector3.MoveTowards(m_DynamicPoint, m_StaticPoint, m_Speed * Time.deltaTime);
        m_LineRenderer.SetPosition(0, m_StaticPoint);
        m_LineRenderer.SetPosition(1, m_DynamicPoint);
        if ((m_DynamicPoint - m_StaticPoint).magnitude < 0.1)
        {
            Destroy(gameObject);
        }
    }
}
