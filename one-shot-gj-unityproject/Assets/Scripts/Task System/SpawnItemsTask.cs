using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;
using UnityEngine.EventSystems;

[CreateAssetMenu(fileName = "SpawnItemsEvent", menuName = "GameEvents/SpawnItemsEvent")]
public class SpawnItemsTask : EventBase
{
    [SerializeField] private string UniquePresetSelectorName = "";
    [SerializeField] private float m_MinEventTime = 1;
    [SerializeField] private float m_MaxEventTime = 1;
    [SerializeField] private int m_MinPresetsCount = 1;
    [SerializeField] private int m_MaxPresetsCount = 1;

    private float m_counter = 0.0f;
    PresetSelector m_selector = null;
    List<PresetCollection> m_activeCollections = new List<PresetCollection>();

    bool m_pendingFinish = false;

    public override bool IsPossibleToSpawn(List<EventBase> eventsOnScene)
    {
        if (!base.IsPossibleToSpawn(eventsOnScene))
        {
            return false;
        }

        PresetSelector selector = FindSelector();
        if (selector == null || selector.CheckValidPresetsCount() < m_MinPresetsCount)
        {
            return false;
        }

        return true;
    }

    public override void SetupEvent()
    {
        base.SetupEvent();

        m_pendingFinish = false;
        m_counter = 0.0f;
        m_selector = null;
        m_activeCollections.Clear();
    }

    public override void Initialize()
    {
        base.Initialize();

        m_counter = UnityEngine.Random.Range(m_MinEventTime, m_MaxEventTime);
        m_selector = FindSelector();
        if (m_selector != null)
        {
            int collectionsCount = UnityEngine.Random.Range(m_MinPresetsCount, m_MaxPresetsCount);
            for (int index = 0; index < collectionsCount; index++)
            {
                PresetCollection activatedCollection = m_selector.ActivateRandomPreset();
                if (activatedCollection == null)
                {
                    break;
                }

                m_activeCollections.Add(activatedCollection);
            }
        }
    }
    public override void StartEvent()
    {
        base.StartEvent();

        if (m_selector == null || m_activeCollections.Count == 0)
        {
            SilentExit();
            return;
        }
    }
    public override void FinishEvent()
    {
        base.FinishEvent();
    }
    public override void Tick()
    {
        base.Tick();

        if (m_pendingFinish)
        {
            TryFinish();
            return;
        }

        if (!IsFinished)
        {
            m_counter -= Time.deltaTime;
            if (m_counter < 0)
            {
                TryFinish();
            }
        }
    }

    private PresetSelector FindSelector()
    {
        return ItemsRegistry<PresetSelector>.GetInstance().FindUniqueRegisteredItem(UniquePresetSelectorName);
    }

    private bool TryFinish()
    {
        bool deactivated = true;
        if (m_selector != null)
        {
            List<PresetCollection> remainingPresets = new List<PresetCollection>(m_activeCollections);
            foreach (PresetCollection collection in m_activeCollections)
            {
                if (m_selector.TryDeactivatePreset(collection))
                {
                    remainingPresets.Remove(collection);
                }
            }

            m_activeCollections = remainingPresets;

            if (remainingPresets.Count == 0)
            {
                m_selector = null;
            }
            else
            {
                deactivated = false;
            }
        }

        m_pendingFinish = !deactivated;
        if (!IsFinished && deactivated)
        {
            SucceedEvent();
        }
        return deactivated;
    }
}
