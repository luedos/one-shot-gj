using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;


public class EventBase : ScriptableObject
{
    [SerializeField, Range(0f, 50f)] protected float m_Probability = 1.0f;
    [SerializeField] protected int m_MinLevelStageToSpawn = 0;
    [SerializeField] protected int m_MaxLevelStageToSpawn = 0;
    [SerializeField] protected string m_DamageCouse = "";
    [SerializeField, Range(0, 10)] protected int m_DamageOnFail = 0;
    [SerializeField, Range(0, 50)] protected int m_PointsOnSuccess = 0;
    [SerializeField] protected float m_MinTimeBeetwenEvents = 0;
    [SerializeField] protected EventTypeData m_TypeData;
    public float Probability { get => m_Probability;}
    public bool IsFinished { get; private set; }
    public bool IsStarted { get; private set; }
    public EventTypeData TypeData { get => m_TypeData; }
    public float LastSpawnedTime { get; set; }
    public virtual bool IsPossibleToSpawn(List<EventBase> eventsOnScene)
    {
        if (Time.fixedTime - LastSpawnedTime < m_MinTimeBeetwenEvents)
        {
            return false;
        }

        if (Gamemode.Instance != null)
        {
            bool isMinMet = Gamemode.Instance.GetLevelProgression() >= m_MinLevelStageToSpawn;
            bool isMaxMet = m_MaxLevelStageToSpawn == 0 || Gamemode.Instance.GetLevelProgression() <= m_MaxLevelStageToSpawn;
            if (!isMinMet || !isMaxMet)
            {
                return false;
            }
        }

        if (eventsOnScene.Count(e => e.TypeData.EventType == TypeData.EventType) >= TypeData.MaxEventsCountOnScene)
        {
            return false;
        }

        return true;
    }
    public virtual void Tick()
    {
        if (!IsStarted)
        {
            StartEvent();
            IsStarted = true;
        }
    }

    // This was a bad decision to use scriptable objects..
    public virtual void SetupEvent()
    {
        IsStarted = false;
        IsFinished = false;
    }

    public virtual void Initialize()
    {
        SetupEvent();
    }
    public virtual void StartEvent() {}

    public virtual void PauseEvent() { }
    public virtual void FinishEvent() { }

    protected void SilentExit()
    {
        IsFinished = true;
        Debug.Log($"Silen exit of event {GetType().Name}.");
    }

    protected void FailEvent()
    {
        IsFinished = true;
        Debug.Log($"FAILED event {GetType().Name}.");
        if (Gamemode.Instance != null)
        {
            Gamemode.Instance.DamageHealth(m_DamageOnFail, m_DamageCouse);
        }
    }

    protected void SucceedEvent()
    {
        IsFinished = true;
        Debug.Log($"SUCCEED event {GetType().Name}.");
        if(GlobalAudioPlayer.GetInstance())
        {
            GlobalAudioPlayer.GetInstance().PlayClip(GlobalAudioPlayer.GetInstance().Success);
        }
        if (Gamemode.Instance != null)
        {
            Gamemode.Instance.AddScore(m_PointsOnSuccess);
        }
    }
}
