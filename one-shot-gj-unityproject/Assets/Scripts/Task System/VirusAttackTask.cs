using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(fileName = "VirusAttack", menuName = "GameEvents/VirusAttack")]
public class VirusAttackTask : EventBase, IComputerLocker
{
    [SerializeField, Range(0.0f, 30.0f)] private float m_MinStrikeTime = 10.0f;
    [SerializeField, Range(0.0f, 30.0f)] private float m_MaxStrikeTime = 10.0f;
    [SerializeField, Range(0, 10)] private int m_MinComputersCount = 2;
    [SerializeField, Range(0, 10)] private int m_MaxComputersCount = 2;
    [SerializeField, Range(0, 10)] private int m_MinConnectionCollections = 0;
    [SerializeField, Range(0, 10)] private int m_MaxConnectionCollections = 0;
    [SerializeField] private string m_ConnectionsSelectorName = "";

    [SerializeField] private Color m_VirusAttackColor = Color.magenta;

    private ComputerTimer m_counter = new ComputerTimer();
    private bool m_isConnected = false;
    private bool m_pendingFinish = false;
    private bool m_pendingResult = false;

    private PresetSelector m_connectionsSelector = null;
    private List<Computer> m_lockedComputers = new List<Computer>();
    private List<ConnectionNetItem> m_lockedConnections = new List<ConnectionNetItem>();
    private List<PresetCollection> m_connectionsCollections = new List<PresetCollection>();

    public override bool IsPossibleToSpawn(List<EventBase> _eventsOnScene)
    {
        return base.IsPossibleToSpawn(_eventsOnScene) && ItemsRegistry<Computer>.GetInstance().GetFreeComputersCount() >= m_MinComputersCount;
    }
    public override void SetupEvent()
    {
        base.SetupEvent();

        m_counter.Reset();
        m_isConnected = false;
        m_pendingFinish = false;
        m_pendingResult = false;

        m_connectionsSelector = null;
        m_lockedComputers.Clear();
        m_lockedConnections.Clear();
        m_connectionsCollections.Clear();
    }

    public override void Initialize()
    {
        base.Initialize();

        m_counter.CurrentValue = UnityEngine.Random.Range(m_MinStrikeTime, m_MaxStrikeTime);
        m_counter.MaxValue = m_counter.CurrentValue;
        m_counter.Color = m_VirusAttackColor;
        m_connectionsSelector = FindConnectionsSelector();
        ComputerColors virusColors = null;
        if (ComputersColorDatabase.GetInstance() != null)
        {
            virusColors = ComputersColorDatabase.GetInstance().GetVirusColors();
        }

        int computersCount = UnityEngine.Random.Range(m_MinComputersCount, m_MaxComputersCount);
        if (computersCount > 0)
        {
            List<Computer> freeComputers = ItemsRegistry<Computer>.GetInstance().GetFreeComputers();

            while (freeComputers.Count > 0)
            {
                int computerIndex = UnityEngine.Random.Range(0, freeComputers.Count);
                Computer computer = freeComputers[computerIndex];
                freeComputers.RemoveAt(computerIndex);

                computer.LockComputer(this);
                computer.SetVirus(true);
                computer.SetColors(virusColors);
                m_lockedComputers.Add(computer);

                if (m_lockedComputers.Count >= computersCount)
                {
                    break;
                }
            }
        }

        if (m_connectionsSelector != null)
        {
            int connectionsCollections = UnityEngine.Random.Range(m_MinConnectionCollections, m_MaxConnectionCollections);
            for (int index = 0; index < connectionsCollections; ++index)
            {
                PresetCollection collection = m_connectionsSelector.ActivateRandomPreset();
                if (collection == null)
                {
                    break;
                }

                m_connectionsCollections.Add(collection);
                ConnectionNetItem[] connectionItems = collection.GetComponentsInChildren<ConnectionNetItem>();
                foreach (ConnectionNetItem item in connectionItems)
                {
                    item.LockComputer(this);
                    item.SetVirus(true);
                    item.SetColors(virusColors);
                    m_lockedConnections.Add(item);
                }
            }
        }
    }

    public override void StartEvent()
    {
        base.StartEvent();

        if (m_lockedComputers.Count == 0 && m_lockedConnections.Count == 0 && m_connectionsCollections.Count == 0)
        {
            UnlockEverything();
            SilentExit();
            return;
        }

        m_isConnected = IsComputersConnected();

        foreach (Computer computer in m_lockedComputers)
        {
            computer.SetConnected(m_isConnected);
        }

        foreach (ConnectionNetItem connection in m_lockedConnections)
        {
            connection.SetConnected(m_isConnected);
        }
        if(GlobalAudioPlayer.GetInstance())
        {
            GlobalAudioPlayer.GetInstance().PlayClip(GlobalAudioPlayer.GetInstance().NewVirus);
        }
    }

    public override void Tick()
    {
        base.Tick();

        if (m_pendingFinish)
        {
            TryFinish(m_pendingResult);
            return;
        }

        UpdateConnected();

        m_counter.CurrentValue -= Time.deltaTime;
        if (m_counter.CurrentValue < 0.0f)
        {
            TryFinish(!m_isConnected);
        }

        foreach (Computer computer in m_lockedComputers)
        {
            computer.SetTimer(m_counter);
        }

        foreach (ConnectionNetItem connection in m_lockedConnections)
        {
            connection.SetTimer(m_counter);
        }
    }

    public override void FinishEvent()
    {
        base.FinishEvent();
    }

    public void ClasterChanged(NetClaster _claster)
    {
        UpdateConnected();
    }

    public void UpdateConnected()
    {
        bool wasConnected = m_isConnected;
        m_isConnected = IsComputersConnected();

        if (wasConnected != m_isConnected)
        {
            foreach (Computer computer in m_lockedComputers)
            {
                computer.SetConnected(m_isConnected);
            }

            foreach (ConnectionNetItem connection in m_lockedConnections)
            {
                connection.SetConnected(m_isConnected);
            }
        }
    }

    private bool IsComputersConnected()
    {
        foreach (Computer lockedComputer in m_lockedComputers)
        {
            NetClaster claster = lockedComputer.GetClaster();
            if (claster == null)
            {
                continue;
            }

            foreach (NetInteractive potentialComputer in claster.GetInteractives())
            {
                Computer computer = potentialComputer as Computer;
                if (computer != null && !computer.IsVirus())
                {
                    return true;
                }
            }
        }

        foreach (ConnectionNetItem lockedConnection in m_lockedConnections)
        {
            NetClaster claster = lockedConnection.GetClaster();
            if (claster == null)
            {
                continue;
            }

            foreach (NetInteractive potentialComputer in claster.GetInteractives())
            {
                Computer computer = potentialComputer as Computer;
                if (computer != null && !computer.IsVirus())
                {
                    return true;
                }
            }
        }

        return false;
    }

    private bool UnlockEverything()
    {
        foreach (Computer computer in m_lockedComputers)
        {
            computer.UnlockComputer(this);
        }
        m_lockedComputers.Clear();

        foreach (ConnectionNetItem connection in m_lockedConnections)
        {
            connection.UnlockComputer(this);
        }
        m_lockedConnections.Clear();


        bool deactivated = true;
        if (m_connectionsSelector != null)
        {
            List<PresetCollection> remainingPresets = new List<PresetCollection>(m_connectionsCollections);
            foreach (PresetCollection collection in m_connectionsCollections)
            {
                if (m_connectionsSelector.TryDeactivatePreset(collection))
                {
                    remainingPresets.Remove(collection);
                }
            }

            if (remainingPresets.Count == 0)
            {
                m_connectionsCollections.Clear();
                m_connectionsSelector = null;
            }
            else
            {
                m_connectionsCollections = remainingPresets;
                deactivated = false;
            }
        }

        return deactivated;
    }

    private bool TryFinish(bool _success)
    {
        m_pendingResult = _success;

        bool deactivated = UnlockEverything();

        m_pendingFinish = !deactivated;
        if (!IsFinished && deactivated)
        {
            if (_success)
            {
                if (GlobalAudioPlayer.GetInstance())
                {
                    GlobalAudioPlayer.GetInstance().PlayClip(GlobalAudioPlayer.GetInstance().Fail);
                }
                SucceedEvent();
            }
            else
            {
                if (GlobalAudioPlayer.GetInstance())
                {
                    GlobalAudioPlayer.GetInstance().PlayClip(GlobalAudioPlayer.GetInstance().Success);
                }
                FailEvent();
            }
        }
        return deactivated;
    }

    private PresetSelector FindConnectionsSelector()
    {
        return ItemsRegistry<PresetSelector>.GetInstance().FindUniqueRegisteredItem(m_ConnectionsSelectorName);
    }
}
