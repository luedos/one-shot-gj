using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ComputerSpawnEvent : EventBase
{
    [SerializeField] private string UniquePresetSelectorName = "";
    [SerializeField] private float m_MinEventTime = 1;
    [SerializeField] private float m_MaxEventTime = 1;
    public float TimeToFinishCounter { get; private set; }
    PresetSelector m_ComputerSelector = null;
    PresetCollection m_ActivatedPreset = null;

    bool m_PendingFinish = false;

    public override void SetupEvent()
    {
        base.SetupEvent();

        m_PendingFinish = false;
        TimeToFinishCounter = 0.0f;
        m_ComputerSelector = null;
        m_ActivatedPreset = null;
    }

    public override void Initialize()
    {
        base.Initialize();

        TimeToFinishCounter = UnityEngine.Random.Range(m_MinEventTime, m_MaxEventTime);
        m_ComputerSelector = FindComputersSelector();
    }
    public override void StartEvent()
    {
        base.StartEvent();

        if (!m_ComputerSelector)
        {
            SilentExit();
        }
        else
        {
            m_ActivatedPreset = m_ComputerSelector.ActivateRandomPreset();
            if(GlobalAudioPlayer.GetInstance())
            {
                GlobalAudioPlayer.GetInstance().PlayClip(GlobalAudioPlayer.GetInstance().ComputerSpawn);
            }
        }
    }
    public override void FinishEvent()
    {
        base.FinishEvent();
    }
    public override void Tick()
    {
        base.Tick();

        if (m_PendingFinish)
        {
            TryFinish(true);
            return;
        }


        if (!IsFinished)
        {
            TimeToFinishCounter -= Time.deltaTime;
            if (TimeToFinishCounter < 0)
            {
                TryFinish(true);
            }
        }
    }
    public override bool IsPossibleToSpawn(List<EventBase> eventsOnScene)
    {
        return base.IsPossibleToSpawn(eventsOnScene) && FindComputersSelector() != null;
    }

    private PresetSelector FindComputersSelector()
    {
        return ItemsRegistry<PresetSelector>.GetInstance().FindUniqueRegisteredItem(UniquePresetSelectorName);
    }

    private bool TryFinish(bool _success)
    {
        bool deactivated = true;
        if (m_ComputerSelector != null)
        {
            deactivated = m_ComputerSelector.TryDeactivatePreset(m_ActivatedPreset);

            if (deactivated)
            {
                m_ActivatedPreset = null;
                m_ComputerSelector = null;
            }
        }

        m_PendingFinish = !deactivated;
        if (!IsFinished && deactivated)
        {
            if (_success)
            {
                SucceedEvent();
            }
            else
            {
                FailEvent();
            }
        }
        return deactivated;
    }
}
