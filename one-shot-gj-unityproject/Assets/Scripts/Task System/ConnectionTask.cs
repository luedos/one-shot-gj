using System.Collections.Generic;
using System.Linq;
using UnityEngine;

[CreateAssetMenu(fileName = "ConnectionTask", menuName = "GameEvents/ConnectionTask")]
public class ConnectionTask : EventBase, IComputerLocker
{
    [SerializeField, Range(0.0f, 30.0f)] private float m_MinConversationTime = 1.0f;
    [SerializeField, Range(0.0f, 30.0f)] private float m_MaxConversationTime = 1.0f;
    [SerializeField, Range(0.0f, 30.0f)] private float m_MinStartingFailTime = 5.0f;
    [SerializeField, Range(0.0f, 30.0f)] private float m_MaxStartingFailTime = 5.0f;
    [SerializeField, Range(0.0f, 30.0f)] private float m_FailTime = 5.0f;
    [SerializeField, Range(2, 10)] private int m_ComputersCount = 2;
    [SerializeField, Range(0, 5)] private int m_MinAdditionalConnectionCollections = 0;
    [SerializeField, Range(0, 5)] private int m_MaxAdditionalConnectionCollections = 0;
    [SerializeField] private string m_AdditionalConnectionsSelectorName = "";
    [SerializeField] private Color m_SuccessColor = Color.green;
    [SerializeField] private Color m_FailedColor = Color.red;

    private ComputerTimer m_failedCounter = new ComputerTimer();
    private ComputerTimer m_successCounter = new ComputerTimer();
    private bool m_isConnected = false;
    private bool m_pendingFinish = false;
    private bool m_pendingResult = false;

    private ComputerColors m_lockedComputerColors = null;
    private PresetSelector m_connectionsSelector = null;
    private List<Computer> m_lockedComputers = new List<Computer>();
    private List<ConnectionNetItem> m_lockedConnections = new List<ConnectionNetItem>();
    private List<PresetCollection> m_connectionsCollections = new List<PresetCollection>();

    public override bool IsPossibleToSpawn(List<EventBase> _eventsOnScene)
    {
        return base.IsPossibleToSpawn(_eventsOnScene) && ItemsRegistry<Computer>.GetInstance().GetFreeComputersCount() >= m_ComputersCount;
    }
    public override void SetupEvent()
    {
        base.SetupEvent();

        m_failedCounter.Reset();
        m_successCounter.Reset();
        m_isConnected = false;
        m_pendingFinish = false;
        m_pendingResult = false;

        m_lockedComputerColors = null;
        m_connectionsSelector = null;
        m_lockedComputers.Clear();
        m_lockedConnections.Clear();
        m_connectionsCollections.Clear();
    }

    public override void Initialize()
    {
        base.Initialize();

        if (ComputersColorDatabase.GetInstance() != null)
        {
            m_lockedComputerColors = ComputersColorDatabase.GetInstance().LockColor();
        }

        m_failedCounter.CurrentValue = UnityEngine.Random.Range(m_MinStartingFailTime, m_MaxStartingFailTime);
        m_successCounter.CurrentValue = UnityEngine.Random.Range(m_MinConversationTime, m_MaxConversationTime);

        m_failedCounter.MaxValue = m_failedCounter.CurrentValue;
        m_successCounter.MaxValue = m_successCounter.CurrentValue;

        m_failedCounter.Color = m_FailedColor;
        m_successCounter.Color = m_SuccessColor;

        m_connectionsSelector = FindConnectionsSelector();

        if (m_ComputersCount > 0)
        {
            List<Computer> freeComputers = ItemsRegistry<Computer>.GetInstance().GetFreeComputers();

            while (freeComputers.Count > 0)
            {
                int computerIndex = UnityEngine.Random.Range(0, freeComputers.Count);
                Computer computer = freeComputers[computerIndex];
                freeComputers.RemoveAt(computerIndex);

                computer.LockComputer(this);
                computer.SetColors(m_lockedComputerColors);
                computer.SetVirus(false);
                m_lockedComputers.Add(computer);

                if (m_lockedComputers.Count >= m_ComputersCount)
                {
                    break;
                }
            }
        }

        if (m_connectionsSelector != null)
        {
            int additionalConnectionsCollections = UnityEngine.Random.Range(m_MinAdditionalConnectionCollections, m_MaxAdditionalConnectionCollections);
            for (int index = 0; index < additionalConnectionsCollections; ++index)
            {
                PresetCollection collection = m_connectionsSelector.ActivateRandomPreset();
                if (collection == null)
                {
                    break;
                }

                m_connectionsCollections.Add(collection);
                ConnectionNetItem[] connectionItems = collection.GetComponentsInChildren<ConnectionNetItem>();
                foreach (ConnectionNetItem item in connectionItems)
                {
                    item.LockComputer(this);
                    item.SetColors(m_lockedComputerColors);
                    item.SetVirus(false);
                    m_lockedConnections.Add(item);
                }
            }
        }
    }

    public override void StartEvent()
    {
        base.StartEvent();

        if (m_lockedComputers.Count == 0 && m_lockedConnections.Count == 0 && m_connectionsCollections.Count == 0)
        {
            UnlockEverything();
            SilentExit();
            return;
        }

        m_isConnected = IsComputersConnected();

        foreach (Computer computer in m_lockedComputers)
        {
            computer.SetConnected(m_isConnected);
        }
        foreach (ConnectionNetItem connection in m_lockedConnections)
        {
            connection.SetConnected(m_isConnected);
        }
        if(GlobalAudioPlayer.GetInstance())
        {
            GlobalAudioPlayer.GetInstance().PlayClip(GlobalAudioPlayer.GetInstance().NewConnectionTask);
        }
        var source = m_lockedComputers.First().GetAudioSource();
        if(source && GlobalAudioPlayer.GetInstance())
        {
            source.clip = GlobalAudioPlayer.GetInstance().DataTransfer;
            source.loop = true;
            source.Stop();
        }    
    }

    public override void Tick()
    {
        base.Tick();

        if (m_pendingFinish)
        {
            TryFinish(m_pendingResult);
            return;
        }

        ComputerTimer mainTimer = null;

      if (!m_isConnected)
        {
            m_lockedComputers.First().GetAudioSource()?.Pause();
            m_failedCounter.CurrentValue -= Time.deltaTime;
            if (m_failedCounter.CurrentValue < 0.0f)
            {
                m_lockedComputers.First().GetAudioSource()?.Stop();
                TryFinish(false);
                return;
            }
            mainTimer = m_failedCounter;
        }
        else
        {
            if(!m_lockedComputers.First().GetAudioSource().isPlaying 
                && ItemsRegistry<Computer>.GetInstance()
                .GetActiveItems()
                .Count(c => c.GetAudioSource().isPlaying) < 3)
            {
                m_lockedComputers.First().GetAudioSource()?.Play();
            }
            m_successCounter.CurrentValue -= Time.deltaTime;
            if (m_successCounter.CurrentValue < 0.0f)
            {
                m_lockedComputers.First().GetAudioSource()?.Stop();
                TryFinish(true);
                return;
            }
            mainTimer = m_successCounter;
        }

        foreach (Computer computer in m_lockedComputers)
        {
            computer.SetTimer(mainTimer);
        }

        foreach (ConnectionNetItem connection in m_lockedConnections)
        {
            connection.SetTimer(mainTimer);
        }
    }

    public override void FinishEvent()
    {
        base.FinishEvent();

        if (ComputersColorDatabase.GetInstance() != null && m_lockedComputerColors != null)
        {
            ComputersColorDatabase.GetInstance().UnlockColor(m_lockedComputerColors);
        }
    }

    public void ClasterChanged(NetClaster _claster)
    {
        bool wasConnected = m_isConnected;
        m_isConnected = IsComputersConnected();

        // If connection status changed, and we are connected now, reset fail timer.
        if (wasConnected != m_isConnected && m_isConnected)
        {
            m_failedCounter.CurrentValue = m_FailTime;
            m_failedCounter.MaxValue = m_FailTime;
        }

        if (wasConnected != m_isConnected)
        {
            foreach (Computer computer in m_lockedComputers)
            {
                computer.SetConnected(m_isConnected);
            }

            foreach (ConnectionNetItem connection in m_lockedConnections)
            {
                connection.SetConnected(m_isConnected);
            }
        }
    }

    private bool IsComputersConnected()
    {
        if (m_lockedComputers.Count == 0)
        {
            return true;
        }

        NetClaster claster = m_lockedComputers[0].GetClaster();
        if (claster == null)
        {
            return false;
        }

        foreach (Computer computer in m_lockedComputers)
        {
            if (!claster.HasInteractive(computer))
            {
                return false;
            }
        }

        foreach (ConnectionNetItem connection in m_lockedConnections)
        {
            if (!claster.HasInteractive(connection))
            {
                return false;
            }
        }

        return true;
    }

    private bool UnlockEverything()
    {
        foreach (Computer computer in m_lockedComputers)
        {
            computer.UnlockComputer(this);
        }
        m_lockedComputers.Clear();

        foreach (ConnectionNetItem connection in m_lockedConnections)
        {
            connection.UnlockComputer(this);
        }
        m_lockedConnections.Clear();


        bool deactivated = true;
        if (m_connectionsSelector != null)
        {
            List<PresetCollection> remainingPresets = new List<PresetCollection>(m_connectionsCollections);
            foreach (PresetCollection collection in m_connectionsCollections)
            {
                if (m_connectionsSelector.TryDeactivatePreset(collection))
                {
                    remainingPresets.Remove(collection);
                }
            }

            if (remainingPresets.Count == 0)
            {
                m_connectionsCollections.Clear();
                m_connectionsSelector = null;
            }
            else
            {
                m_connectionsCollections = remainingPresets;
                deactivated = false;
            }
        }

        return deactivated;
    }

    private bool TryFinish(bool _success)
    {
        m_pendingResult = _success;

        bool deactivated = UnlockEverything();

        m_pendingFinish = !deactivated;
        if (!IsFinished && deactivated)
        {
            if (_success)
            {
                if (GlobalAudioPlayer.GetInstance())
                {
                    GlobalAudioPlayer.GetInstance().PlayClip(GlobalAudioPlayer.GetInstance().Success);
                }
                SucceedEvent();
            }
            else
            {
                if (GlobalAudioPlayer.GetInstance())
                {
                    GlobalAudioPlayer.GetInstance().PlayClip(GlobalAudioPlayer.GetInstance().Fail);
                }

                FailEvent();
            }
        }
        return deactivated;
    }

    private PresetSelector FindConnectionsSelector()
    {
        return ItemsRegistry<PresetSelector>.GetInstance().FindUniqueRegisteredItem(m_AdditionalConnectionsSelectorName);
    }
}
