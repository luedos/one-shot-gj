using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EventSystem : MonoBehaviour
{
    [SerializeField]
    [Range(1, 50)]
    private float m_GenerateEventDelayMin = 5;

    [SerializeField]
    [Range(1, 50)]
    private float m_GenerateEventDelayMax = 10;

    [SerializeField]
    [Range(1, 50)]
    private float m_GenerateEventDelayAfterNoEventsGenerated = 5;

    [SerializeField]
    private List<EventBase> m_PossibleEvents;

    Coroutine m_GenerateConnectCoroutine;
    List<EventBase> m_EventsOnScene = new List<EventBase>();

    public bool IsGeneratingTasks { get => m_GenerateConnectCoroutine != null; }

    [ContextMenu("Start Event Generation")]
    public void StartEventGeneration()
    {
        StopEventGeneration();

        m_GenerateConnectCoroutine = StartCoroutine(DoGenerate());
    }
    [ContextMenu("Stop Event Generation")]

    public void StopEventGeneration()
    {
        if (IsGeneratingTasks)
        {
            StopCoroutine(m_GenerateConnectCoroutine);
            m_GenerateConnectCoroutine = null;
        }
    }
    IEnumerator DoGenerate()
    {
        yield return new WaitForSeconds(2.0f);

        while (true)
        {
            bool IsEventSpawned = false;

            List<EventBase> selectedEvents = m_PossibleEvents.FindAll(e => !m_EventsOnScene.Contains(e) && e.IsPossibleToSpawn(m_EventsOnScene));

            float totalProbability = 0.0f;

            foreach (EventBase _event in selectedEvents)
            {
                totalProbability += _event.Probability;
            }

            float chance = UnityEngine.Random.Range(0.0f, totalProbability);

            float incrementalChance = 0.0f;
            foreach (EventBase _event in selectedEvents)
            {
                incrementalChance += _event.Probability;
                if (incrementalChance >= chance)
                {
                    _event.Initialize();
                    m_EventsOnScene.Add(_event);
                    _event.LastSpawnedTime = Time.fixedTime;
                    IsEventSpawned = true;
                    Debug.Log("Event spawned type " + _event.TypeData.EventType.ToString());
                    break;
                }
            }

            if (IsEventSpawned)
            {
                yield return new WaitForSeconds(UnityEngine.Random.Range(m_GenerateEventDelayMin, m_GenerateEventDelayMax));
            }
            else
            {
                yield return new WaitForSeconds(m_GenerateEventDelayAfterNoEventsGenerated);

            }
        }
    }
    private void Update()
    {

        List<EventBase> finishedEvents= new List<EventBase>();
        foreach (EventBase _event in m_EventsOnScene)
        {
            _event.Tick();
            if(_event.IsFinished)
            {
                finishedEvents.Add(_event);
            }
        }

        foreach (EventBase _event in finishedEvents)
        {
            Debug.Log("Event Completed type " + _event.TypeData.EventType.ToString());
            _event.FinishEvent();
            m_EventsOnScene.Remove(_event);
        }

    }

    private void Start()
    {
        foreach (EventBase ev in m_PossibleEvents)
        {
            ev.SetupEvent();
            ev.LastSpawnedTime = -1000.0f;
        }

        StartEventGeneration();
    }

    private void OnDestroy()
    {
        StopEventGeneration();
    }
}
