using System.Collections;
using System.Collections.Generic;
using UnityEngine;


public enum EventType
{ 
    ConnectionTask,
    ComputerSpawn,
    WallsSpawn,
    MenInTheMiddleAttack,
    VirusAttack,
    EventCount,
}
[CreateAssetMenu(fileName = "BaseType", menuName = "EventTypeData")]
public class EventTypeData : ScriptableObject
{
    [SerializeField]
    protected EventType m_EventType;

    [SerializeField]
    protected uint m_MaxEventsCountOnScene = 10;

    public uint MaxEventsCountOnScene { get=> m_MaxEventsCountOnScene; }

    public EventType EventType { get => m_EventType; }

}
