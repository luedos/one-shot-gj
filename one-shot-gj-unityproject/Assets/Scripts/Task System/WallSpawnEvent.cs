using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;
using UnityEngine.EventSystems;

public class WallSpawnEvent : EventBase
{
    [SerializeField] private string UniquePresetSelectorName = "";
    [SerializeField] private float m_MinEventTime = 1;
    [SerializeField] private float m_MaxEventTime = 1;
    public float TimeToFinishCounter { get;private set; }
    PresetSelector m_WallSelector = null;
    PresetCollection m_ActivatedPreset = null;

    bool m_PendingFinish = false;

    public override void SetupEvent()
    {
        base.SetupEvent();

        m_PendingFinish = false;
        TimeToFinishCounter = 0.0f;
        m_WallSelector = null;
        m_ActivatedPreset = null;
    }

    public override void Initialize()
    {
        base.Initialize();

        TimeToFinishCounter = UnityEngine.Random.Range(m_MinEventTime, m_MaxEventTime);
        m_WallSelector = FindWallsSelector();
    }
    public override void StartEvent()
    {
        if (!m_WallSelector)
        {
            SilentExit();
        }
        else
        {
            m_ActivatedPreset = m_WallSelector.ActivateRandomPreset();
            if(GlobalAudioPlayer.GetInstance())
            {
                GlobalAudioPlayer.GetInstance().PlayClip(GlobalAudioPlayer.GetInstance().WallSpawn);
            }
        }
    }
    public override void FinishEvent()
    {
        if (GlobalAudioPlayer.GetInstance())
        {
            GlobalAudioPlayer.GetInstance().PlayClip(GlobalAudioPlayer.GetInstance().WallDespawn);
        }
        base.FinishEvent();
    }
    public override void Tick()
    {
        base.Tick();

        if (m_PendingFinish)
        {
            TryFinish(true);
            return;
        }

        if(!IsFinished)
        {
            TimeToFinishCounter -= Time.deltaTime;
            if (TimeToFinishCounter < 0)
            {
                TryFinish(true);
            }
        }
    }
    public override bool IsPossibleToSpawn(List<EventBase> eventsOnScene)
    {
        return base.IsPossibleToSpawn(eventsOnScene) && FindWallsSelector() != null;
    }

    private PresetSelector FindWallsSelector()
    {
        return ItemsRegistry<PresetSelector>.GetInstance().FindUniqueRegisteredItem(UniquePresetSelectorName);
    }

    private bool TryFinish(bool _success)
    {
        bool deactivated = true;
        if (m_WallSelector != null)
        {
            deactivated = m_WallSelector.TryDeactivatePreset(m_ActivatedPreset);

            if (deactivated)
            {
                m_ActivatedPreset = null;
                m_WallSelector = null;
            }
        }

        m_PendingFinish = !deactivated;
        if (!IsFinished && deactivated)
        {
            if (_success)
            {
                SucceedEvent();
            }
            else
            {
                FailEvent();
            }
        }
        return deactivated;
    }
}
