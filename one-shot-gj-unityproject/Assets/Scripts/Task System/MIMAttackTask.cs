using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using static UnityEngine.RuleTile.TilingRuleOutput;

[CreateAssetMenu(fileName = "MIMAttack", menuName = "GameEvents/MIMAttack")]
public class MIMAttackTask : EventBase, IComputerLocker
{
    [SerializeField, Range(0.0f, 30.0f)] private float m_MinStrikeTime = 10.0f;
    [SerializeField, Range(0.0f, 30.0f)] private float m_MaxStrikeTime = 10.0f;
    [SerializeField, Range(0, 10)] private int m_MinItemsSpawned = 1;
    [SerializeField, Range(0, 10)] private int m_MaxItemsSpawned = 1;
    [SerializeField] private ConnectionNetItem m_ConnectionNetItemPrefab;
    [SerializeField] private float m_CollisionCheckRadius = 2.0f;
    [SerializeField] private LayerMask m_CollisionCheckLayerMask;

    [SerializeField] private Color m_VirusAttackColor = Color.magenta;

    private ComputerTimer m_counter = new ComputerTimer();
    private bool m_isConnected = false;

    private List<ConnectionNetItem> m_spawnedItems = new List<ConnectionNetItem>();

    public override bool IsPossibleToSpawn(List<EventBase> _eventsOnScene)
    {
        return base.IsPossibleToSpawn(_eventsOnScene) && ItemsRegistry<SpiderWeb>.GetInstance().GetActiveItemsCount() >= m_MinItemsSpawned;
    }

    public override void SetupEvent()
    {
        base.SetupEvent();

        m_counter.Reset();
        m_isConnected = false;

        m_spawnedItems.Clear();
    }

    public override void Initialize()
    {
        base.Initialize();

        m_counter.CurrentValue = UnityEngine.Random.Range(m_MinStrikeTime, m_MaxStrikeTime);
        m_counter.MaxValue = m_counter.CurrentValue;
        m_counter.Color = m_VirusAttackColor;

        ComputerColors virusColors = null;
        if (ComputersColorDatabase.GetInstance() != null)
        {
            virusColors = ComputersColorDatabase.GetInstance().GetVirusColors();
        }

        int spawnCount = UnityEngine.Random.Range(m_MinItemsSpawned, m_MaxItemsSpawned);
        
        if (spawnCount > 0)
        {
            List<SpiderWeb> allWeb = new List<SpiderWeb>(ItemsRegistry<SpiderWeb>.GetInstance().GetActiveItems());
            while (allWeb.Count > 0 && m_spawnedItems.Count < spawnCount)
            {
                int randomWebIndex = UnityEngine.Random.Range(0, allWeb.Count);
                SpiderWeb chosenWeb = allWeb[randomWebIndex];
                allWeb.RemoveAt(randomWebIndex);

                Vector2 webSpawnPosition = (chosenWeb.GetLeftPoint().transform.position + chosenWeb.GetRightPoint().transform.position) / 2;

                if (CheckPosition(webSpawnPosition))
                {
                    ConnectionNetItem spawnedItem = Object.Instantiate<ConnectionNetItem>(m_ConnectionNetItemPrefab, (Vector3)webSpawnPosition, Quaternion.identity, null);
                    spawnedItem.LockComputer(this);
                    spawnedItem.SetVirus(true);
                    spawnedItem.SetColors(virusColors);
                    m_spawnedItems.Add(spawnedItem);
                }
            }
        }
    }

    public override void StartEvent()
    {
        base.StartEvent();

        if (m_spawnedItems.Count == 0)
        {
            SilentExit();
            return;
        }

        m_isConnected = IsComputersConnected();

        foreach (ConnectionNetItem connection in m_spawnedItems)
        {
            connection.SetConnected(m_isConnected);
        }
        if (GlobalAudioPlayer.GetInstance())
        {
            GlobalAudioPlayer.GetInstance().PlayClip(GlobalAudioPlayer.GetInstance().NewVirus);
        }
    }

    public override void Tick()
    {
        base.Tick();
        
        UpdateConnected();

        m_counter.CurrentValue -= Time.deltaTime;
        if (m_counter.CurrentValue < 0.0f)
        {
            if (m_isConnected)
            {
                FailEvent();
            }
            else
            {
                SucceedEvent();
            }

            return;
        }

        foreach (ConnectionNetItem connection in m_spawnedItems)
        {
            connection.SetTimer(m_counter);
        }
    }

    public override void FinishEvent()
    {
        base.FinishEvent();

        foreach (ConnectionNetItem item in m_spawnedItems)
        {
            Object.Destroy(item.gameObject);
        }

        m_spawnedItems.Clear();
    }

    public void ClasterChanged(NetClaster _claster)
    {
        UpdateConnected();
    }

    public void UpdateConnected()
    {
        bool wasConnected = m_isConnected;
        m_isConnected = IsComputersConnected();

        if (wasConnected != m_isConnected)
        {
            foreach (ConnectionNetItem connection in m_spawnedItems)
            {
                connection.SetConnected(m_isConnected);
            }
        }
    }

    private bool IsComputersConnected()
    {
        foreach (ConnectionNetItem lockedConnection in m_spawnedItems)
        {
            NetClaster claster = lockedConnection.GetClaster();
            if (claster == null)
            {
                continue;
            }

            foreach (NetInteractive potentialComputer in claster.GetInteractives())
            {
                Computer computer = potentialComputer as Computer;
                if (computer != null && !computer.IsVirus())
                {
                    return true;
                }
            }
        }

        return false;
    }

    private bool CheckPosition(Vector3 _position)
    {
        RaycastHit2D[] hits = Physics2D.CircleCastAll(_position, m_CollisionCheckRadius, Vector2.one, 0.0f, m_CollisionCheckLayerMask.value);
        foreach (RaycastHit2D hit in hits)
        {
            if (hit.collider != null)
            {
                return false;
            }
        }

        return true;
    }
}

