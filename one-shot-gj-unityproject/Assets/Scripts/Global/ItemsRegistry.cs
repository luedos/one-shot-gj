using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public interface IRegistryEntry
{
    public string GetRegistryName();
}

public class ItemsRegistry<TypeT> where TypeT : class, IRegistryEntry
{
    protected List<TypeT> m_activeItems = new List<TypeT>();

    static private ItemsRegistry<TypeT> m_instance = null;
    static public ItemsRegistry<TypeT> GetInstance()
    {
        if (m_instance == null)
        {
            m_instance = new ItemsRegistry<TypeT>();
        }

        return m_instance;
    }

    public void RegisterItem(TypeT _item)
    {
        if (!m_activeItems.Contains(_item))
        {
            string itemName = _item.GetRegistryName();
            if (!string.IsNullOrEmpty(itemName) && FindUniqueRegisteredItem(itemName) != null)
            {
                Debug.LogError($"Double registration of unique registry item of type '{nameof(TypeT)}' with name '{itemName}'.");
            }

            m_activeItems.Add(_item);
        }
    }

    public void UnregisterItem(TypeT _item)
    {
        m_activeItems.Remove(_item);
    }

    public TypeT FindUniqueRegisteredItem(string _name)
    {
        if (string.IsNullOrEmpty(_name))
        {
            return null;
        }

        return m_activeItems.Find(item => item.GetRegistryName() == _name);
    }

    public IEnumerable<TypeT> GetActiveItems()
    {
        return m_activeItems;
    }

    public int GetActiveItemsCount()
    {
        return m_activeItems.Count;
    }

    public List<TypeT> GetItemsHowTheFuckCSharpIsWorking()
    {
        return m_activeItems;
    }
}
