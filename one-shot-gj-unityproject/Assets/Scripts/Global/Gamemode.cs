using System.Collections;
using Data;
using TMPro;
using Ui;
using UnityEngine;
using UnityEngine.Events;

public class Gamemode : MonoBehaviour
{
    static public Gamemode Instance { get; private set; }

    [SerializeField] private UnityEvent<int> m_OnHealthUpdated;
    [SerializeField] private UnityEvent<int> m_OnScoreUpdated;
    [SerializeField] private UnityEvent<int> m_OnMaxScoreUpdated;
    [SerializeField] private UnityEvent m_GameOver;
    [SerializeField] private int m_MaxHealth = 10;
    [SerializeField] private float m_SecondsPerProgressionLevel = 30.0f;
    [SerializeField] private TextMeshProUGUI m_ScoreText = null;
    [SerializeField] private TextMeshProUGUI m_LivesCountText = null;
    [SerializeField] private TextMeshProUGUI m_LastDamageCauseText = null;
    [SerializeField] private TextMeshProUGUI m_ProgressionText = null;

    private int m_maxScore = 0;
    private string m_lastDamageCause = "";
    private int m_currentScore = 0;
    private int m_currentHealth = 0;
    private bool m_gameover = false;

    private void Awake()
    {
        m_currentHealth = m_MaxHealth;
        m_currentScore = 0;
        m_gameover = false;
        Time.timeScale = 1;
        AudioListener.volume = 1;

        if (Instance == null)
        {
            Instance = this;
            LoadMaxScore();
        }
    }

    private void OnDestroy()
    {
        if (Instance == this)
        {
            SaveScore();
            Instance = null;
        }
    }

    private void OnEnable()
    {
        m_ScoreText.text = $"Current Score: {m_currentScore}";
        m_LivesCountText.text = $"Number Of Lives: {m_currentHealth}";
        m_LastDamageCauseText.text = "*Last Damage Log*";
        m_ProgressionText.text = $"Progression: {GetLevelProgression()}";
    }

    private void Update()
    {
        m_ProgressionText.text = $"Progression: {GetLevelProgression()}";
    }

    private void LoadMaxScore()
    {
        m_maxScore = Mathf.Max(PersistentData.Instance.MaxScore.Value, m_currentScore);
        m_OnMaxScoreUpdated.Invoke(m_maxScore);
    }

    private void SaveScore()
    {
        m_maxScore = Mathf.Max(m_currentScore, m_maxScore);
        PersistentData.Instance.MaxScore.Value = m_maxScore;
    }

    public void AddScore(int _score)
    {
        if (m_gameover)
        {
            return;
        }

        m_currentScore += _score;
        m_ScoreText.text = $"Current Score: {m_currentScore}";
        m_OnScoreUpdated.Invoke(m_currentScore);
    }

    public int GetCurrentScore()
    {
        return m_currentScore;
    }

    public string GetLastDamageCause()
    {
        return m_lastDamageCause;
    }

    public int GetMaxScore()
    {
        return m_maxScore;
    }

    public void GameOver()
    {
        if (m_gameover)
        {
            return;
        }

        Time.timeScale = 0;
        StartCoroutine(TweenVolume());

        m_gameover = true;
        SaveScore();
        m_GameOver.Invoke();
        UiManager.Instance.GetScreen<GameOver>(ScreenType.GameOver).Show();
    }

    private IEnumerator TweenVolume()
    {
        float from = AudioListener.volume;
        float to = 0;
        float duration = 1f;
        float timePassed = 0f;

        while (timePassed < duration)
        {
            timePassed += Time.unscaledDeltaTime;
            float value = Mathf.Lerp(from, to, timePassed / duration);
            AudioListener.volume = Mathf.Clamp(value, 0, 100);
            yield return null;
        }

        Debug.Log("Volume tuned down");
    }

    public int GetLevelProgression()
    {
        return Mathf.FloorToInt(Time.timeSinceLevelLoad / m_SecondsPerProgressionLevel);
    }

    public void DamageHealth(int _amount, string _source)
    {
        m_currentHealth -= _amount;
        m_OnHealthUpdated.Invoke(m_currentHealth);
        m_LivesCountText.text = $"Number Of Lives: {m_currentHealth}";
        m_LastDamageCauseText.text = $"Last Damage Caused by:\n{_source}";
        m_lastDamageCause = _source;

        if (m_currentHealth <= 0)
        {
            GameOver();
        }
    }
}