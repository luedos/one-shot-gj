using Scenes;
using UnityEngine.SceneManagement;

namespace Global
{
    public class GameModeSwitcher
    {
        private static GameModeSwitcher instance;

        public static GameModeSwitcher Instance
        {
            get { return instance ??= new GameModeSwitcher(); }
        }

        public void LoadGameLevel()
        {
            SceneManager.LoadScene(SceneNames.GameMode);
        }
    }
}