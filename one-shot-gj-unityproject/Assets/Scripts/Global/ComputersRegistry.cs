using System.Collections;
using System.Collections.Generic;
using Unity.VisualScripting;
using UnityEngine;

public static class ComputersRegistry
{
    public static List<Computer> GetFreeComputers(this ItemsRegistry<Computer> _registry)
    {
        return _registry.GetItemsHowTheFuckCSharpIsWorking().FindAll(computer => !computer.IsLocked());
    }

    public static int GetFreeComputersCount(this ItemsRegistry<Computer> _registry)
    {
        int count = 0;
        foreach (Computer computer in _registry.GetActiveItems())
        {
            if (!computer.IsLocked())
            {
                ++count;
            }
        }

        return count;
    }
}
