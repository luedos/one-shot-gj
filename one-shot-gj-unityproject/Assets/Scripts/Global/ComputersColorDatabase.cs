using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[Serializable]
public class ComputerColors
{
    public Color connectedMainColor;
    public Color connectedAccentColor;
    public Color disconnectedMainColor;
    public Color disconnectedAccentColor;
}

public class ComputersColorDatabase : MonoBehaviour
{
    [SerializeField] private List<ComputerColors> m_PossibleColors = new List<ComputerColors>();
    [SerializeField] private ComputerColors m_DefaultColors = new ComputerColors();
    [SerializeField] private ComputerColors m_VirusColors = new ComputerColors();

    private List<ComputerColors> m_lockedColors = new List<ComputerColors>();
    private static ComputersColorDatabase s_instance = null;

    public static ComputersColorDatabase GetInstance()
    {
        return s_instance;
    }

    private void Awake()
    {
        if (s_instance == null)
        {
            s_instance = this;
        }
    }

    private void OnDestroy()
    {
        if (s_instance == this)
        {
            s_instance = null;
        }
    }

    public ComputerColors LockColor()
    {
        if (m_PossibleColors.Count > 0)
        {
            int randomIndex = UnityEngine.Random.Range(0, m_PossibleColors.Count);
            ComputerColors colors = m_PossibleColors[randomIndex];
            m_PossibleColors.RemoveAt(randomIndex);

            m_lockedColors.Add(colors);
            return colors;
        }

        return m_DefaultColors;
    }

    public void UnlockColor(ComputerColors _colors)
    {
        if (m_lockedColors.Contains(_colors))
        {
            m_lockedColors.Remove(_colors);
            m_PossibleColors.Add(_colors);
        }
    }

    public ComputerColors GetVirusColors()
    {
        return m_VirusColors;
    }
}
