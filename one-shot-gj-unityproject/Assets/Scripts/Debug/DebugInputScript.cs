using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DebugInputScript : MonoBehaviour
{
    [SerializeField] private PresetSelector m_Selector = null;

    private List<PresetCollection> m_activatedTransforms = new List<PresetCollection>();

    private void Update()
    {
        if (Input.GetKeyDown(KeyCode.Y))
        {
            PresetCollection activatedPreset = m_Selector.ActivateRandomPreset();
            if (activatedPreset != null)
            {
                Debug.Log($"Activated preset {activatedPreset.name}.");
                m_activatedTransforms.Add(activatedPreset);
            }
            else
            {
                Debug.Log($"Failed to activate any preset.");
            }
        }

        if (Input.GetKeyDown(KeyCode.U) && m_activatedTransforms.Count > 0)
        {
            PresetCollection activatedPreset = m_activatedTransforms[UnityEngine.Random.Range(0, m_activatedTransforms.Count)];
            m_Selector.TryDeactivatePreset(activatedPreset);
            m_activatedTransforms.Remove(activatedPreset);
            Debug.Log($"Deactivated preset {activatedPreset}.");
        }
    }
}
