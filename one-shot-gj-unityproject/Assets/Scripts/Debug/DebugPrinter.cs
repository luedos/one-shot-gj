using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DebugPrinter : MonoBehaviour
{
    [SerializeField] private string m_ToPrint = "";

    public void Print(string _text)
    {
        Debug.Log($"[{gameObject.name}] ({m_ToPrint}) : {_text}");
    }
}
