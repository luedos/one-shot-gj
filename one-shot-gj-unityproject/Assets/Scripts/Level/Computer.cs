using System.Collections;
using System.Collections.Generic;
using System.Reflection.Emit;
using UnityEditor;
using UnityEngine;

public interface IComputerLocker
{
    public void ClasterChanged(NetClaster _claster);
}
public class ComputerTimer
{
    public float MaxValue;
    public float MinValue;
    public float CurrentValue;
    public Color Color;
    public void Reset()
    {
        MaxValue = 0;
        MinValue = 0;
        CurrentValue = 0;
        Color = Color.white;
    }
}
[RequireComponent(typeof(AudioSource))]
public class Computer : SpawnableNetItem, IRegistryEntry
{
    [SerializeField] private string m_UniqueRegistryName = "";
    [SerializeField] private SpriteRenderer m_BaseSprite = null;
    [SerializeField] private SpriteRenderer m_AccentSprite = null;

    private Color m_defaultBaseColor = Color.white;
    private Color m_defaultAccentColor = Color.white;
    private ComputerColors m_colors = null;

    private IComputerLocker m_locker = null;
    private bool m_isConnected = false;
    private bool m_isVirus = false;
    private float m_timer = 0.0f;
    private ProgressBarController m_ProgressBarController = null;
    private AudioSource m_AudioSource = null;

    public AudioSource GetAudioSource() => m_AudioSource;
    public void LockComputer(IComputerLocker _lock)
    {
        if (m_locker == null)
        {
            m_locker = _lock;
            m_ProgressBarController.Show();
        }
    }

    public void UnlockComputer(IComputerLocker _lock)
    {
        if (_lock == m_locker)
        {
            m_locker = null;
            m_isConnected = false;
            m_timer = 0.0f;
            m_isVirus = false;

            m_BaseSprite.color = m_defaultBaseColor;
            m_AccentSprite.color = m_defaultAccentColor;

            m_ProgressBarController.Hide();
        }
    }

    public bool IsLocked()
    {
        return m_locker != null;
    }

    public void SetConnected(bool _connected)
    {
        m_isConnected = _connected;
    }

    public void SetVirus(bool _virus)
    {
        m_isVirus = _virus;
    }

    public bool IsVirus()
    {
        return IsLocked() && m_isVirus;
    }

    public void SetTimer(ComputerTimer _timer)
    {
        m_ProgressBarController.SetupRange(_timer.MaxValue, _timer.MinValue);
        m_timer = _timer.CurrentValue;
        m_ProgressBarController.SetState(m_timer);
        m_ProgressBarController.SetProgressColor(_timer.Color);
    }

    public void SetColors(ComputerColors _colors)
    {
        m_colors = _colors;
    }

    public string GetRegistryName()
    {
        return m_UniqueRegistryName;
    }

    private new void Update()
    {
        base.Update();

        if (IsLocked() && m_colors != null)
        {
            m_BaseSprite.color = m_isConnected ? m_colors.connectedMainColor : m_colors.disconnectedMainColor;
            m_AccentSprite.color = m_isConnected ? m_colors.connectedAccentColor : m_colors.disconnectedAccentColor;
        }
    }

    private void Awake()
    {
        m_AudioSource = GetComponent<AudioSource>();
    }
    private void Start()
    {
        m_defaultBaseColor = m_BaseSprite.color;
        m_defaultAccentColor = m_AccentSprite.color;
        m_ProgressBarController = GetComponentInChildren<ProgressBarController>();
    }

    private new void OnEnable()
    {
        base.OnEnable();
        ItemsRegistry<Computer>.GetInstance().RegisterItem(this);
    }

    private new void OnDisable()
    {
        ItemsRegistry<Computer>.GetInstance().UnregisterItem(this);
        base.OnDisable();
    }

    protected override void OnClasterChanged(NetClaster _newClaster)
    {
        if (m_locker != null)
        {
            m_locker.ClasterChanged(_newClaster);
        }
    }
}
