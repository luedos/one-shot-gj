using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PresetItemCollision : PresetItem
{
    [SerializeField] private Transform m_LeftPoint = null;
    [SerializeField] private Transform m_RightPoint = null;
    [SerializeField] private float m_CollisionRadius = 1.0f;
    [SerializeField] private LayerMask m_CollisionMask;

    private void OnDrawGizmosSelected()
    {
        Gizmos.DrawWireSphere(m_LeftPoint.position, m_CollisionRadius);
        Gizmos.DrawWireSphere(m_RightPoint.position, m_CollisionRadius);
        
        Vector2 startPoint = m_LeftPoint.position;
        Vector2 direction = m_RightPoint.position - m_LeftPoint.position;

        Vector2 rightDirection = new Vector2(direction.y, -direction.x).normalized;
        Vector2 rightStartPoint = startPoint + rightDirection;
        Vector2 leftStartPoint = startPoint - rightDirection;
        Gizmos.DrawLine(leftStartPoint, leftStartPoint + direction);
        Gizmos.DrawLine(rightStartPoint, rightStartPoint + direction);
    }

    public override bool IsPresetValid()
    {
        Vector2 direction = m_RightPoint.position - m_LeftPoint.position;
        RaycastHit2D[] hits = Physics2D.CircleCastAll(m_LeftPoint.position, m_CollisionRadius, direction, direction.magnitude, m_CollisionMask.value);
        foreach (RaycastHit2D hit in hits)
        {
            if (hit.collider != null && !IsDescendant(hit.transform))
            {
                return false;
            }
        }

        return true;
    }

    private bool IsDescendant(Transform _transform)
    {
        Transform transofrmIt = _transform;
        while (transofrmIt != null)
        {
            if (transofrmIt == transform)
            {
                return true;
            }

            transofrmIt = transofrmIt.parent;
        }

        return false;
    }
}
