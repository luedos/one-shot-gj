using System;
using System.Collections;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using UnityEngine;


public class PresetSelector : MonoBehaviour, IRegistryEntry
{
    [SerializeField] private string m_UniqueRegistryName = "";

    [InspectorButton("OnPopulateCollectionsFromChildren", ButtonWidth = 300)]
    public bool m_populateCollectionsFromChildren = false;

    [Serializable] 
    private class SpawnPresetInfo
    {
        public PresetCollection collection = null;
        public float probability = 1.0f;
    }

    [SerializeField] private SpawnPresetInfo[] m_Presets = new SpawnPresetInfo[0];

    private List<SpawnPresetInfo> m_activatedPresets = new List<SpawnPresetInfo>();

    public PresetCollection ActivateRandomPreset()
    {
        SpawnPresetInfo chosenPreset = GetRandomPreset();
        if (chosenPreset != null)
        {
            chosenPreset.collection.ActivateItems();
            m_activatedPresets.Add(chosenPreset);
            return chosenPreset.collection;
        }

        return null;
    }

    public bool TryDeactivatePreset(PresetCollection _transform)
    {
        SpawnPresetInfo preset = FindPreset(_transform);
        if (preset == null  || !IsActive(preset))
        {
            return true;
        }

        bool deactivated = preset.collection.TryDeactivateItems();
        if (deactivated)
        {
            m_activatedPresets.Remove(preset);
        }

        return deactivated;
    }

    public bool HasValidPresets()
    {
        foreach (SpawnPresetInfo preset in m_Presets)
        {
            if (!IsActive(preset) && preset.collection.IsValid())
            {
                return true;
            }
        }

        return false;
    }

    public int CheckValidPresetsCount()
    {
        int count = 0;
        foreach (SpawnPresetInfo preset in m_Presets)
        {
            if (!IsActive(preset) && preset.collection.IsValid())
            {
                ++count;
            }
        }

        return count;
    }

    public string GetRegistryName()
    {
        return m_UniqueRegistryName;
    }

    private void OnEnable()
    {
        ItemsRegistry<PresetSelector>.GetInstance().RegisterItem(this);
    }

    private void OnDisable()
    {
        ItemsRegistry<PresetSelector>.GetInstance().UnregisterItem(this);
    }

    private void OnPopulateCollectionsFromChildren()
    {
        PresetCollection[] collections = GetComponentsInChildren<PresetCollection>();
        SpawnPresetInfo[] newPresets = new SpawnPresetInfo[collections.Length];

        for (int index = 0; index < collections.Length; index++)
        {
            newPresets[index] = new SpawnPresetInfo();
            newPresets[index].collection = collections[index];
        }

        foreach (SpawnPresetInfo oldPreset in m_Presets)
        {
            SpawnPresetInfo newPreset = newPresets.FirstOrDefault(preset => preset.collection == oldPreset.collection);
            if (newPreset != null)
            {
                newPreset.probability = oldPreset.probability;
            }
        }

        m_Presets = newPresets;
    }

    private SpawnPresetInfo GetRandomPreset()
    {
        float totalProbability = 0.0f;
        List<SpawnPresetInfo> validPresets = new List<SpawnPresetInfo>();
        foreach (SpawnPresetInfo preset in m_Presets)
        {
            if (!IsActive(preset) && preset.collection.IsValid())
            {
                validPresets.Add(preset);
                totalProbability += preset.probability;
            }
        }

        float chance = UnityEngine.Random.Range(0.0f, totalProbability);

        float incrementalChance = 0.0f;
        foreach (SpawnPresetInfo preset in validPresets)
        {
            incrementalChance += preset.probability;
            if (incrementalChance >= chance)
            {
                return preset;
            }
        }

        return null;
    }

    private SpawnPresetInfo FindPreset(PresetCollection _collection)
    {
        foreach (SpawnPresetInfo preset in m_Presets)
        {
            if (preset.collection == _collection)
            {
                return preset;
            }
        }

        return null;
    }

    private bool IsActive(SpawnPresetInfo _preset)
    {
        return m_activatedPresets.Contains(_preset);
    }
}
