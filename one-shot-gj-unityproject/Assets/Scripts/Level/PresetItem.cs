using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;

public class PresetItem : MonoBehaviour
{
    [SerializeField] private UnityEvent m_OnItemActivated = null;
    [SerializeField] private UnityEvent m_OnItemDeactivated = null;

    private bool m_active = false;


    public virtual void ActivateItem()
    {
        m_OnItemActivated.Invoke();
        m_active = true;
    }

    public virtual bool TryDeactivateItem()
    {
        m_OnItemDeactivated.Invoke();
        m_active = false;
        return true;
    }

    public bool IsPresetActive()
    {
        return m_active;
    }

    public virtual bool IsPresetValid()
    {
        return true;
    }
}
