using System.Linq;
using UnityEngine;

public class PresetCollection : MonoBehaviour
{
    [SerializeField] private bool m_IsPresetActive = true;
    [SerializeField] private int m_MinProgressionStage = 0;

    private PresetItem[] m_items = null;

    public virtual bool IsValid()
    {
        if (!m_IsPresetActive)
        {
            return false;
        }

        if (Gamemode.Instance != null && Gamemode.Instance.GetLevelProgression() < m_MinProgressionStage)
        {
            return false;
        }

        return m_items.All(item => item.IsPresetValid());
    }

    public void ActivateItems()
    {
        foreach (PresetItem item in m_items)
        {
            item.ActivateItem();
        }
    }

    public bool TryDeactivateItems()
    { 
        bool deactivated = true;
        foreach (PresetItem item in m_items)
        {
            if (!item.IsPresetActive())
            {
                continue;
            }

            bool itemDeactivated = item.TryDeactivateItem();
            deactivated = deactivated && itemDeactivated;
        }

        return deactivated;
    }

    private void Start()
    {
        m_items = GetComponentsInChildren<PresetItem>();
    }

}
