using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ComputerPresetItem : PresetItemCollision
{
    [SerializeField] private Computer m_Computer = null;

    public override bool TryDeactivateItem()
    {
        if(m_Computer != null && m_Computer.IsLocked())
        {
            return false;
        }
        else
        {
            return base.TryDeactivateItem();
        }
    }
}
