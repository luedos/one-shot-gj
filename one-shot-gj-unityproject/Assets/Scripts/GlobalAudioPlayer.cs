using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[RequireComponent(typeof(AudioSource))]
public class GlobalAudioPlayer : MonoBehaviour
{
    public AudioClip NewConnectionTask;
    public AudioClip SpiderWebSpawn;
    public AudioClip SpiderWebDestroy;
    public AudioClip Success;
    public AudioClip Fail;
    public AudioClip NewVirus;
    public AudioClip WallSpawn;
    public AudioClip WallDespawn;
    public AudioClip ComputerSpawn;
    public AudioClip WebMiss;
    public AudioClip DataTransfer;

    AudioSource m_Source;
    static GlobalAudioPlayer s_Instance = null;
    public static GlobalAudioPlayer GetInstance() => s_Instance;
    private void Awake()
    {
        m_Source = GetComponent<AudioSource>();
        s_Instance = this;
    }
    public void PlayClip(AudioClip clip)
    {
        if(clip && m_Source)
        {
            m_Source.PlayOneShot(clip);
        }
    }
}
