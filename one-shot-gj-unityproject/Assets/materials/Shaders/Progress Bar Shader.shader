Shader "Custom/Progress Bar"
{
    Properties
    {
       _MainTex("Sprite Texture", 2D) = "white" {}
       _BackgroundColor("Background Color", Color) = (0,0,0,1)
       _ProgressColor("Progress Color", Color) = (1,0,0,1)
       _Progress("Progress", Range(-0.01,1)) = 0.1
    }
        SubShader
       {
           Tags
           {
               "RenderType" = "Opaque"
               "Queue" = "Transparent+1"
           }

           Pass
           {
               ZWrite Off
               Blend SrcAlpha OneMinusSrcAlpha

               CGPROGRAM
               #pragma vertex vert
               #pragma fragment frag
               #pragma multi_compile DUMMY PIXELSNAP_ON

               sampler2D _MainTex;
               float4 _BackgroundColor;
               float4 _ProgressColor;
               float _Progress;

               struct Vertex
               {
                   float4 vertex : POSITION;
                   float2 uv_MainTex : TEXCOORD0;
                   float2 uv2 : TEXCOORD1;
               };

               struct Fragment
               {
                   float4 vertex : POSITION;
                   float2 uv_MainTex : TEXCOORD0;
                   float2 uv2 : TEXCOORD1;
               };

               Fragment vert(Vertex v)
               {
                   Fragment o;

                   o.vertex = UnityObjectToClipPos(v.vertex);
                   o.uv_MainTex = v.uv_MainTex;
                   o.uv2 = v.uv2;

                   return o;
               }

               float4 frag(Fragment IN) : COLOR
               {
                   float4 o = _BackgroundColor;
                   if (IN.uv_MainTex.x < _Progress)
                   {
                        o = _ProgressColor;
                   }

                   return o;
               }

               ENDCG
           }
       }
}