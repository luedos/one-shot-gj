Shader "Custom/Sprite Circle Appear"
{
    Properties
    {
       _MainTex("Sprite Texture", 2D) = "white" {}
       _Color("Tint", Color) = (0, 0, 0, 1)
       _Progress("Progress", Range(-0.01,1)) = 0.1
       _Center("CenterUV", Vector) = (0.5, 0.5, 0, 1)
    }
        SubShader
       {
           Tags
           {
               "RenderType" = "Opaque"
               "Queue" = "Transparent+1"
           }

           Pass
           {
               ZWrite Off
               Blend SrcAlpha OneMinusSrcAlpha

               CGPROGRAM
               #pragma vertex vert
               #pragma fragment frag
               #pragma multi_compile DUMMY PIXELSNAP_ON

               sampler2D _MainTex;
               float4 _Color;
               float _Progress;
               float4 _Center;

               struct Vertex
               {
                   float4 vertex : POSITION;
                   float2 uv_MainTex : TEXCOORD0;
                   float2 uv2 : TEXCOORD1;
                   fixed4 color : COLOR;
               };

               struct Fragment
               {
                   float4 vertex : POSITION;
                   float2 uv_MainTex : TEXCOORD0;
                   float2 uv2 : TEXCOORD1;
                   fixed4 color : COLOR;
               };

               Fragment vert(Vertex v)
               {
                   Fragment o;

                   o.vertex = UnityObjectToClipPos(v.vertex);
                   o.uv_MainTex = v.uv_MainTex;
                   o.uv2 = v.uv2;
                   o.color = v.color;
                   return o;
               }

               float4 circle(float2 uv, float2 pos, float rad, float3 color) 
               {
                   float d = length(pos - uv) - rad;
                   float t = clamp(d, 0.0, 1.0);
                   return float4(color, 1.0 - t);
               }

               float4 frag(Fragment IN) : COLOR
               {


                   //float4 o = _Color;
                   //float4 color = circle(IN.uv_MainTex.x, float2(0.5f, 0.5f), _Progress, tex2D(_MainTex, IN.uv_MainTex));
                   float d = length(IN.uv_MainTex - _Center.xy);
                   float a = smoothstep(0, _Progress, d);
                   //float4 col = tex2D(_MainTex, IN.uv_MainTex);
                   //col.a *= a;
                  // return col;
                   float4 col = tex2D(_MainTex, IN.uv_MainTex) * _Color * IN.color;
                   if (d >= _Progress)
                   {
                       col.w *= 1-smoothstep(0, 0.3, d - _Progress);
                       return col;
                   }
                   return col;
                   //return color;
               }

               ENDCG
           }
       }
}
