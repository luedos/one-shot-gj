using System.Collections.Generic;
using UnityEngine;

namespace OneShot.Net
{
    public class NetPool<T> where T : ISimpleNet
    {
        private readonly List<T> _pooled = new();
        private readonly INetSpawner<T> _spawner;

        public int Count => _pooled.Count;

        public NetPool(INetSpawner<T> spawner)
        {
            _spawner = spawner;
        }

        public T Get()
        {
            if (_pooled.Count > 0)
            {
                Debug.Log("Get Cached");
                return GetCached();
            }

            Debug.Log("Get new");
            return _spawner.SpawnNet();
        }

        private T GetCached()
        {
            int lastIndex = _pooled.Count - 1;
            T net = _pooled[lastIndex];
            net.Enable();
            _pooled.RemoveAt(lastIndex);
            return net;
        }

        public void Return(T net)
        {
            Debug.Log("Return");
            net.Disable();
            _pooled.Add(net);
        }
    }
}