using UnityEngine;
using UnityEngine.InputSystem;

namespace OneShot.Net
{
    public partial class NetSpawnFromMouseDrag  : MonoBehaviour
    {
        [SerializeField] private InputActionReference _pointerDragAction;

        private INetSpawner<ICompositeNet> _spawner;
        private Camera _mainCamera;

        private State _state;

        public void Initialize(INetSpawner<ICompositeNet> spawner, Camera mainCamera)
        {
            _mainCamera = mainCamera;
            _spawner = spawner;
        }

        private void OnEnable()
        {
            _state = State.Idle;
            _pointerDragAction.action.started += HandleSpawnStart;
            _pointerDragAction.action.performed += HandleSpawnPerform;
        }

        private void OnDisable()
        {
            _pointerDragAction.action.started -= HandleSpawnStart;
            _pointerDragAction.action.performed -= HandleSpawnPerform;
        }

        private void HandleSpawnStart(InputAction.CallbackContext ctx)
        {
            Vector2 worldPosition = _mainCamera.ScreenToWorldPoint(ctx.ReadValue<Vector2>());
            ICompositeNet net = _spawner.SpawnNet();
            net.SetFromTo(worldPosition, worldPosition);
            _state = State.CreateSpawned(worldPosition, net);
        }

        private void HandleSpawnPerform(InputAction.CallbackContext ctx)
        {
            Vector2 screenPos = ctx.ReadValue<Vector2>();

            // quick fix for case when mouse drag action returns default after user clicks instead of drag 
            if (screenPos == Vector2.zero)
            {
                return;
            }

            Vector2 worldPosition = _mainCamera.ScreenToWorldPoint(ctx.ReadValue<Vector2>());
            _state.Net.SetTo(worldPosition);
        }
    }
}