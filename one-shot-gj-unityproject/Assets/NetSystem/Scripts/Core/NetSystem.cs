using Pathfinding;

namespace OneShot.Net
{
    public static class NetSystem
    {
        public static PathManager Path { get; private set; }

        public static void Initialize(NetSystemConfig config, FunnelModifier funnel)
        {
            Path = new PathManager(config, funnel);
        }
    }
}