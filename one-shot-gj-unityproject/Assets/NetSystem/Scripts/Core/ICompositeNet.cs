using UnityEngine;

namespace OneShot.Net
{
    public interface ICompositeNet : INet
    {
        void SetTo(Vector2 to);
    }
}