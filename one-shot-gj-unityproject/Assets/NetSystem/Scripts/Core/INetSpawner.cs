namespace OneShot.Net
{
    public interface INetSpawner<T> where T : INet
    {
        T SpawnNet();
    }
}