using UnityEngine;

namespace OneShot.Net
{
    public interface ISimpleNet : INet
    {
        public void ToBlockedState();
        public void ToNormalState();
    }
}