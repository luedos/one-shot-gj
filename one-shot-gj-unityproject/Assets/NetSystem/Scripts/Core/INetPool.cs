namespace OneShot.Net
{
    public interface INetPool<T> where T : INet
    {
        int Count { get; }
        T Get();
        void Return(T net);
    }
}