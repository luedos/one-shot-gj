using UnityEngine;

namespace OneShot.Net
{
    public interface INet
    {
        Vector2 From { get; }
        Vector2 To { get; }
        void SetFromTo(Vector2 from, Vector2 to);
        void Enable();
        void Disable();
    }
}