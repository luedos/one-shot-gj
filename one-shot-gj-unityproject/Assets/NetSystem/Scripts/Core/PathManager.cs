using System;
using Pathfinding;
using UnityEngine;

namespace OneShot.Net
{
    public class PathManager
    {
        private readonly NetSystemConfig _config;
        private readonly FunnelModifier _funnelModifier;

        public PathManager(NetSystemConfig config, FunnelModifier funnelModifier)
        {
            _funnelModifier = funnelModifier;
            _config = config;
        }

        public bool BoxCastBypassHit(Vector3 lastPoint, Vector3 preLastPoint, Vector3 lastUnblockedTo, RaycastHit2D hit, out Vector2
            result)
        {
            Vector3 dir = lastUnblockedTo - preLastPoint;
            Vector3 dirNormalized = dir.normalized;
            Vector2 origin = preLastPoint + dir / 2;
            Vector2 size = new(dir.magnitude, 0.01f);

            float angle = Vector2.Angle(Vector3.right, dirNormalized) * Math.Sign(dir.x);

            // todo: somehow cast only for collided object
            return BoxCastFromBothAngles(lastPoint, lastUnblockedTo, hit, origin, preLastPoint, size, angle, out result);
        }

        private bool BoxCastFromBothAngles(Vector2 lastPoint, Vector3 lastUnblockedTo, RaycastHit2D hit,
            Vector2 origin, Vector3 preLastPoint, Vector2 size, float angle, out Vector2 result)
        {
            Vector2 boxcastDir = ((Vector2) hit.collider.bounds.center - origin).normalized;

            Debug.DrawLine(origin, origin + boxcastDir * 5, Color.red, 0.05f);
            Debug.DrawLine(preLastPoint, lastUnblockedTo, Color.blue, 0.05f);

            bool rectBlocked = IsRectBlocked(origin, size, angle);
            bool invRectBlocked = IsRectBlocked(origin, size, -angle);
            
            if (rectBlocked && invRectBlocked)
            {
                result = default;
                return false;
            }

            if (rectBlocked)
            {
                result = BoxCast(origin, size, -angle, boxcastDir);
                return true;
            }

            if(invRectBlocked)
            {
                result = BoxCast(origin, size, angle, boxcastDir);
                return true;
            }

            Vector2 boxCastPoint1 = BoxCast(origin, size, angle, boxcastDir);
            Vector2 boxCastPoint2 = BoxCast(origin, size, -angle, boxcastDir);

            result = (boxCastPoint1 - lastPoint).magnitude > (boxCastPoint2 - lastPoint).magnitude
                ? boxCastPoint2
                : boxCastPoint1;

            return true;
        }

        private bool IsRectBlocked(Vector2 origin, Vector2 size, float angle)
        {
            float w = size.x * 0.5f;
            float h = size.y * 0.5f;
            Quaternion q = Quaternion.AngleAxis(angle, new Vector3(0, 0, 1));

            Vector2 p1 = (Vector2) (q * new Vector2(w, -h)) + origin;
            Vector2 p2 = (Vector2) (q * new Vector2(-w, -h)) + origin;

            return Physics2D.Linecast(p1, p2, _config.ObstacleFilter);
        }


        public Vector2 BoxCast(Vector2 origin, Vector2 size, float angle, Vector2 boxcastDir)
        {
            BoxCastDrawer.Draw(default, origin, size, angle, boxcastDir);
            RaycastHit2D boxCastHit = Physics2D.BoxCast(origin, size, angle, boxcastDir);
            Vector2 boxCastPoint = boxCastHit.point + (-boxcastDir) * _config.RaycastPointPushout;
            return boxCastPoint;
        }

        public bool IsPathBlocked(Vector2 from, Vector2 to, out RaycastHit2D hit)
        {
            hit = Physics2D.Linecast(from, to, _config.ObstacleFilter);
            hit.point += hit.normal * _config.RaycastPointPushout;
 
            return hit.collider != null;
        }

        public bool IsPointBlocked(Vector2 from)
        {
            return Physics2D.OverlapPoint(from, _config.ObstacleFilter) != null;
        }

        public bool IsAnyBlocked(Vector2 from, Vector2 to)
        {
            return IsPointBlocked(from) || IsPointBlocked(to);
        }
    }
}