using UnityEngine;

namespace OneShot.Net
{
    public class ComplexCompositeNetProvider : MonoBehaviour, INetSpawner<ICompositeNet>
    {
        [SerializeField] private GameObjectFactory<ComplexCompositeNet> _compositeNetViewFactory;
        
        private NetPool<ISimpleNet> _basicNetsPool;

        public void Initialize(NetPool<ISimpleNet> basicNetsPool)
        {
            _basicNetsPool = basicNetsPool;
        }

        public ICompositeNet SpawnNet()
        {
            ComplexCompositeNet view = _compositeNetViewFactory.Create();
            view.Initialize(_basicNetsPool);
            return view;
        }
    }
}