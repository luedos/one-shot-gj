using UnityEngine;

namespace OneShot.Net
{
    public class BasicNetView : MonoBehaviour, ISimpleNet
    {
        [SerializeField] private float _defaultWidth = 1f;
        [SerializeField] private SpriteRenderer _line;
        [SerializeField] private Color _blockedColor;
        [SerializeField] private Color _normalColor;
        [SerializeField] private Transform _p1;
        [SerializeField] private Transform _p2;

        public Vector2 From { get; private set; }
        public Vector2 To { get; private set; }

        public void SetFromTo(Vector2 from, Vector2 to)
        {
            From = from;
            To = to;

            gameObject.SetActive(from != to);

            Vector2 direction = (to - from).normalized;
            float length = (to - from).magnitude;
            Vector2 center = from / 2 + to / 2;
            transform.position = center;
            transform.localScale = new Vector3(GetWidth(length), length, 1f);
            transform.up = direction;
            _p1.position = From;
            _p2.position = To;
        }

        public void Enable()
        {
            gameObject.SetActive(true);
        }

        public void Disable()
        {
            gameObject.SetActive(false);
        }

        private void Awake()
        {
            _line.color = _normalColor;
        }

        private float GetWidth(float length)
        {
            return _defaultWidth;
        }

        public void ToBlockedState()
        {
            _line.color = _blockedColor;
        }

        public void ToNormalState()
        {
            _line.color = _normalColor;
        }
    }
}