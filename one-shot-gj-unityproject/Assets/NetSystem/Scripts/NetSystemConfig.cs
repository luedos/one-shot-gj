using System;
using UnityEngine;

namespace OneShot.Net
{
    [CreateAssetMenu]
    public class NetSystemConfig : ScriptableObject
    {
        [field: SerializeField] public LayerMask ObstacleFilter { get; private set; }
        [field: SerializeField] public LayerMask TempCheckFilter { get; private set; }

        [field: HideInInspector]
        [field: SerializeField]
        public float DistanceThresholdSq { get; private set; }

        [field: SerializeField] public float DistanceTreshold { get; private set; }
        [field: SerializeField] public float RaycastPointPushout { get; private set; }

        private void OnValidate()
        {
            DistanceThresholdSq = DistanceTreshold * DistanceTreshold;
        }
    }
}