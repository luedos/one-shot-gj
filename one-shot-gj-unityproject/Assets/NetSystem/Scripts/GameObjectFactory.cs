using System;
using UnityEngine;
using Object = UnityEngine.Object;

namespace OneShot.Net
{
    [Serializable]
    public struct GameObjectFactory<T> where T : MonoBehaviour
    {
        [SerializeField] private Transform _parent;
        [SerializeField] private T _prefab;

        public T Create()
        {
            return Object.Instantiate(_prefab, _parent);
        }
    }
}