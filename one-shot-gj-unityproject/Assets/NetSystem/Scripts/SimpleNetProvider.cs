using UnityEngine;

namespace OneShot.Net
{
    public class SimpleNetProvider : MonoBehaviour, INetSpawner<ISimpleNet>
    {
        [SerializeField] private GameObjectFactory<BasicNetView> _netViewFactory;

        public ISimpleNet SpawnNet()
        {
            BasicNetView netBasicView = _netViewFactory.Create();
            return netBasicView;
        }
    }
}