using UnityEngine;

namespace OneShot.Net
{
    public partial class NetSpawnFromMouseDrag
    {
        private readonly struct State
        {
            public readonly Vector2 SpawnPosition;
            public readonly ICompositeNet Net;

            private State(Vector2 spawnPosition, ICompositeNet net)
            {
                SpawnPosition = spawnPosition;
                Net = net;
            }

            public static State CreateSpawned(Vector2 point, ICompositeNet net) => new(point, net);
            public static State Idle => new(Vector2.zero, null);
        }
    }
}