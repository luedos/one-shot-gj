using UnityEngine;

namespace OneShot.Net
{
    public class NetSpawnFromTwoPoints : MonoBehaviour
    {
        [SerializeField] private Transform _p1;
        [SerializeField] private Transform _p2;

        private INetSpawner<ICompositeNet> _spawner;
        private Vector3 _p2LastPos;
        private ICompositeNet _net;

        public void Initialize(INetSpawner<ICompositeNet> spawner)
        {
            _spawner = spawner;
        }

        private void Start()
        {
            Spawn();
        }

        private void Spawn()
        {
            _net = _spawner.SpawnNet();
            _net.SetFromTo(_p1.position,_p2.position);
            _p2LastPos = _p2.position;
        }

        private void Update()
        {
            Vector3 currentPosition = _p2.position;
            
            if (currentPosition != _p2LastPos)
            {
                _p2LastPos = currentPosition;
                _net.SetTo(currentPosition);
            }
        }
    }
}