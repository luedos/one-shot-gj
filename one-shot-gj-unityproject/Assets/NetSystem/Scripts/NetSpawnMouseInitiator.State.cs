using UnityEngine;

namespace OneShot.Net
{
    public partial class NetSpawnFromMouseClick
    {
        private readonly struct State
        {
            public readonly Vector2 SpawnPosition;
            public readonly bool IsPressed;

            private State(Vector2 spawnPosition, bool isPressed)
            {
                SpawnPosition = spawnPosition;
                IsPressed = isPressed;
            }

            public static State CreateFirstPointSelected(Vector2 point) => new(point, true);
            public static State Idle => new(Vector2.zero, false);
        }
    }
}