using System.Collections.Generic;
using UnityEngine;

namespace OneShot.Net
{
    public class ComplexCompositeNet : MonoBehaviour, ICompositeNet
    {
        public Vector2 From
        {
            get => _path[0];
            private set => _path[0] = value;
        }

        public Vector2 To
        {
            get => _path[^1];
            private set => _path[^1] = value;
        }

        private readonly List<ISimpleNet> _subViews = new();
        private NetPool<ISimpleNet> _basicNetPool;
        private Vector3 _lastUnblockedTo;
        private bool _targetPointBlocked;
        private int _byPassDepth;

        private readonly List<Vector3> _path = new();

        public void Initialize(NetPool<ISimpleNet> basicNetPool)
        {
            _basicNetPool = basicNetPool;
        }

        public void SetTo(Vector2 to)
        {
            To = to;

            _targetPointBlocked = NetSystem.Path.IsPointBlocked(to);

            bool lastSegmentBlocked = NetSystem.Path.IsPathBlocked(_path[^2], to, out RaycastHit2D hit);
            if (!lastSegmentBlocked)
            {
                _lastUnblockedTo = to;
            }

            if (_targetPointBlocked)
            {
                DrawSegments(true);
            }
            else
            {
                if (lastSegmentBlocked)
                {
                    BypassObstacle(_lastUnblockedTo, hit);
                }

                DrawSegments(false);
            }
        }

        private void BypassObstacle(Vector3 lastUnblockedTo, RaycastHit2D hit)
        {
            _byPassDepth++;

            if (_byPassDepth > 10)
            {
                return;
            }

            NetSystem.Path.BoxCastBypassHit(To, _path[^2], lastUnblockedTo, hit, out Vector2 boxCastPoint);

            if (Vector2.Distance(hit.point, boxCastPoint) > 0.025)
            {
                _path.Insert(_path.Count - 1, hit.point);
            }

            _path.Insert(_path.Count - 1, boxCastPoint);

            _byPassDepth--;
        }

        public void SetFromTo(Vector2 from, Vector2 to)
        {
            SetSimplePath(from, to);
            _targetPointBlocked = NetSystem.Path.IsAnyBlocked(from, to);

            if (!_targetPointBlocked)
            {
                _lastUnblockedTo = to;
            }

            DrawSegments(_targetPointBlocked);
        }

        private void SetSimplePath(Vector2 from, Vector2 to)
        {
            _path.Clear();
            _path.Add(from);
            _path.Add(to);
        }

        public void Enable()
        {
            for (var i = 0; i < _subViews.Count; i++)
            {
                _subViews[i].Enable();
            }
        }

        public void Disable()
        {
            for (var i = 0; i < _subViews.Count; i++)
            {
                _subViews[i].Disable();
            }
        }

        private void DrawSegments(bool lastSegmentBlocked)
        {
            for (int i = 0; i < _path.Count - 1; i++)
            {
                Vector2 point1 = _path[i];
                Vector2 point2 = _path[i + 1];

                if (i >= _subViews.Count)
                {
                    _subViews.Add(_basicNetPool.Get());
                }

                ISimpleNet subView = _subViews[i];
                subView.SetFromTo(point1, point2);
            }


            for (int i = _path.Count - 1; i < _subViews.Count; i++)
            {
                ISimpleNet subView = _subViews[i];
                _basicNetPool.Return(subView);
                _subViews.RemoveAt(i);
            }

            if (lastSegmentBlocked)
            {
                _subViews[^1].ToBlockedState();
            }
            else
            {
                _subViews[^1].ToNormalState();
            }
        }
    }
}