using UnityEngine;
using UnityEngine.InputSystem;

namespace OneShot.Net
{
    public partial class NetSpawnFromMouseClick : MonoBehaviour
    {
        [SerializeField] private InputActionReference _pointerPositionAction;
        [SerializeField] private InputActionReference _spawnCommandAction;

        private State _state;
        private INetSpawner<ISimpleNet> _spawner;
        private Camera _mainCamera;

        public void Initialize(INetSpawner<ISimpleNet> spawner, Camera mainCamera)
        {
            _mainCamera = mainCamera;
            _spawner = spawner;
        }

        private void OnEnable()
        {
            _state = State.Idle;
            _spawnCommandAction.action.performed += HandleSpawnCommand;
        }

        private void OnDisable()
        {
            _spawnCommandAction.action.performed -= HandleSpawnCommand;
        }

        private void HandleSpawnCommand(InputAction.CallbackContext ctx)
        {
            Vector2 pointerPosition = _pointerPositionAction.action.ReadValue<Vector2>();

            if (!_state.IsPressed)
            {
                _state = State.CreateFirstPointSelected(pointerPosition);
                return;
            }

            InitiateSpawn(_state.SpawnPosition, pointerPosition);
            _state = State.Idle;
        }

        private void InitiateSpawn(Vector2 start, Vector2 end)
        {
            Vector2 worldStart = _mainCamera.ScreenToWorldPoint(start);
            Vector2 worldEnd = _mainCamera.ScreenToWorldPoint(end);

            ISimpleNet net = _spawner.SpawnNet();
            net.SetFromTo(worldStart, worldEnd);
        }
    }
}