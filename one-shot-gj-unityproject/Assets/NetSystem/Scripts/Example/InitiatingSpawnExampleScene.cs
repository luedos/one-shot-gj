using UnityEngine;

namespace OneShot.Net
{
    public class InitiatingSpawnExampleScene : MonoBehaviour
    {
        [SerializeField] private NetSpawnFromMouseClick _spawnFromClick;
        [SerializeField] private SimpleNetProvider _provider;
        [SerializeField] private Camera _mainCamera;

        private void Awake()
        {
            _spawnFromClick.Initialize(_provider, _mainCamera);
        }
    }
}