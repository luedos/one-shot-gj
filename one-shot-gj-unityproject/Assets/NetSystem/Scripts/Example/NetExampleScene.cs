using Pathfinding;
using UnityEngine;

namespace OneShot.Net
{
    public class NetExampleScene : MonoBehaviour
    {
        [SerializeField] private SimpleNetProvider _basicNetProvider;
        [SerializeField] private ComplexCompositeNetProvider _complexCompositeNetProvider;
        [SerializeField] private NetSystemConfig _config;
        [SerializeField] private FunnelModifier _funnel;
        [SerializeField] private NetSpawnFromTwoPoints _spawn;
        
        private void Awake()
        {
            NetSystem.Initialize(_config, _funnel);

            NetPool<ISimpleNet> pool = new(_basicNetProvider);
            _complexCompositeNetProvider.Initialize(pool);
            _spawn.Initialize(_complexCompositeNetProvider);
        }
    }
}