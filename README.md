## Game by OneShot team
This game created for submission for the yearly game jam.

### Resources used:
A* pathfinding package (free version) https://arongranberg.com/astar/download
InspectorButton script (special thanks for u/zaikman) https://www.reddit.com/r/Unity3D/comments/1s6czv/inspectorbutton_add_a_custom_button_to_your/
Titillium Web Google font: https://fonts.google.com/specimen/Titillium+Web?preview.text=How%20to%20play&preview.text_type=custom
